#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <SDL2/SDL.h>
#include "const.h"
#include "struc.h"
#include "generation.h"
#include "fichiers_parse.h"


int choixFonctionParse(const char *fic, unsigned char **orig, char *texte, DimTableaux dim, int *nbVivantes)
{
    char *p = NULL;
    p = strrchr(fic, '.') + 1;
    if(p == NULL) return -1;

    if(!compareExtension(p, "LIF"))
        parseFichierLIF(fic, orig, texte, dim, nbVivantes);
    else
        parseFichierRLE(fic, orig, texte, dim, nbVivantes);

    return 0;
}

int compareExtension(const char * fileExt, const char * extension)
{
    while (*fileExt != '\0' && toupper(*fileExt) == *extension)
    {
        fileExt++;
        extension++;
    }
    return (*(unsigned char *) fileExt) - (*(unsigned char *) extension);
}

void supprimeRetourLigne(char *chaine)
{
    while(*chaine != '\0'){
        if(*chaine == '\r' || *chaine == '\n'){
            *chaine = '\0';
            break;
        }
        chaine++;
    }
}

/* Supprime les espaces. La chaine doit être modifiable ! */
void delSpace(char* str)
{
char c, *p;

    p = str;
    do
        while ((c = *p++) == ' ') ;
    while ((*str++ = c));
    return;
}

int posY(char *chaine, int *y)
{
    char *p = strrchr(chaine, ' ');
    if(p)
        return extraitNombre(p, y);
    else
        printf("posI : p = NULL\n");
    return 0;
}

int posX(char *chaine, int *x)
{
    char *p = strchr(chaine, ' ');
    if(p)
        return extraitNombre(p, x);
    else
        printf("posJ : p = NULL\n");
    return 0;
}

int extraitNombre(char *p, int *n)
{
    int negatif = 0;
    *n = 0;

    while(*p != '\0'){
        p++;
        if(*p == '-')
            negatif = 1;
        else if(*p >= '0' && *p <= '9')
            *n = *n * 10 + *p - '0';
        else if(*p == ' ')
            break;
    }
    if(negatif)
        *n *= -1;
    return 1;
}

/**********************************************************************
 *                                                                    *
 *                Partie pour parser les fichier LIF                  *
 *                                                                    *
 * *******************************************************************/

/* Parse le fichier lif et rempli le tableau. */
int parseFichierLIF(const char *fic, unsigned char **orig, char *texte, DimTableaux dim, int *nbVivantes)
{
    char chaine[NB_CARACTERES];
    chaine[0] = '\0';

    FILE *fichier = fopen(fic, "r");
    if(fichier == NULL){
        printf("fichier non ouvert\n");
        goto Erreur;
    }

    /* test si 1ere ligne commence par #Life 1.05 ou #Life 1.06,
     * sinon pas aux norme .LIF. */
    fgets(chaine, sizeof chaine, fichier);

    supprimeRetourLigne(chaine);

    if(strcmp(chaine, "#Life 1.05") && strcmp(chaine, "#Life 1.06")){
        printf("%s :\nPremière ligne pas aux normes !\n", fic);
        strcat(texte, "   --->   Première ligne pas aux normes !");
        goto Erreur;
    }

    int centre_i, centre_j, tmp_i, tmp_j, pass = 0, x = 0, y = 0;

    centre_i = tmp_i = dim.hauteur / 2;
    centre_j = tmp_j = dim.largeur / 2;

    /* Lis les lignes commentées, règle, position .. */
    while(fgets(chaine, sizeof chaine, fichier) != NULL){

        if(chaine[0] == '#'){

            if(chaine[1] == 'R'){
                printf("%s : Autre règle du jeu de la vie, non gérée. \n", fic);
                strcat(texte, "   --->   Règle non gérée !");
                goto Erreur;
            }
            if(chaine[1] == 'P'){

                tmp_i = centre_i;
                tmp_j = centre_j;

                if(pass)
                    pass = 0;

                if(!posY(chaine, &y))
                    goto Erreur;

                if(!posX(chaine, &x))
                    goto Erreur;

                if(x > centre_j || x < centre_j * -1 ||\
                   y > centre_i || y < centre_i * -1)
                    { pass = 1; continue; }

                tmp_i += y; tmp_j += x;
                x = y = 0;
            }
        }
        else if(chaine[0] == '.' || chaine[0] == '*'){
            if(!pass){
                while(chaine[x] != '\0'){
                    if(chaine[x] == '*'){
                        if(tmp_j + x > centre_j + centre_j - 1)
                            break;
                        if(tmp_i + y > centre_i + centre_i - 1)
                            break;
                        set_cell(orig, dim, tmp_j+x, tmp_i+y);
                        orig[tmp_i+y][tmp_j+x] |= 0x80;
                        *nbVivantes+=1;
                    }
                    ++x;
                }
                x = 0;
                ++y;
            }
        }
        else if(chaine[0] != '\r'){
            printf("%s : Début de ligne pas aux normes. %c .\n", fic, chaine[0]);
            strcat(texte, "   --->   Début de ligne pas aux normes !");
            goto Erreur;
        }
    }

    fclose(fichier);
    return 0;

Erreur:
    if(fichier != NULL) fclose(fichier);
    printf("Fonction: figureDansTableau, fichier: parse.c\n");
    return -1;
}


/*******************************************************************************
 *                                                                             *
 *                  Partie pour parser les fichiers RLE.                       *
 *                                                                             *
 * ****************************************************************************/

/* Parse la 1ere ligne x, y eu rule s'il y a. */
int lignexyrRLE(char *chaine, unsigned int *x, unsigned int *y)
{
    char buffer[20] = {0};
    char *p = NULL;

    /* Parse x. */
    if(*chaine == 'x' || *chaine == 'X'){
        chaine+=2;
        while(*chaine >= '0' && *chaine <= '9'){
            *x = *x * 10 + *chaine - '0';
            chaine++;
        }
    }

    p = strchr(chaine, ',');
    chaine++;
    p++;

    /* parse y. */
    if(*p == 'y' || *p == 'Y'){
        p+=2;
        while(*p >= '0' && *p <= '9'){
            *y = *y * 10 + *p - '0';
            p++;
        }
    }
    else{
        printf("Erreur fichier : y non trouvé!\n");
        return -1;
    }

    /* Pas de règle, B3/S23 par défaut. */
    if(*p == '\0') return 0;

    chaine = p;
    p = strchr(chaine, ',');
    int j = 0;

    while(p != NULL){
        chaine++;
        p++;

        /* parse partie texte, Ex : rule, step ...*/
        while(!(*p >= '0' && *p <= '9')){
            if(*p == '=')
                break;
            buffer[j++] = *p++;
        }
        /* Ajoute le zéro de fin. */
        buffer[j] = '\0';

        /* Si rule , on parse la regle. */
        if(!strcmp(buffer, "rule")){
            chaine = p;
            p = strchr(chaine, '=');
            j = 0;

            do{
                p++;
                if(*p == ':') /* Si rule est sous cette forme 'b3/s23:100' */
                    break;    /* On sors de la boucle.                     */
                if((*p >= '1' && *p <= '9') || *p == '/'){
                    buffer[j++] = *p;
                }
            }while(*p != ',' && *p != '\0');

            buffer[j] = '\0';
            /* La règle est parsée. On quitte la boucle. */
            break;
        }
        else{
            j = 0;
        }

        chaine = p;
        p = strchr(chaine, ',');
    }

    if(p == NULL) return 0;

    /* On compare la régle. */
    if(!strcmp(buffer, "3/23")) return 0;
    if(!strcmp(buffer, "23/3")) return 0;

    printf("Règle non gérée\n");
    return -1;
}

/* Parse le fichier rle et rempli le tableau. */
int parseFichierRLE(const char *fic, unsigned char **orig, char *texte, DimTableaux dim, int *nbVivantes)
{
    char chaine[NB_CARACTERES];
    chaine[0] = '\0';

    FILE *fichier = fopen(fic, "r");
    if(fichier == NULL){
        printf("Erreur ouverture fichier\n");
        goto Erreur;
    }

    unsigned int x = 0, y = 0;
    int pos = 0, px = 0, py = 0;
    char *p = NULL;

    while(fgets(chaine, sizeof chaine, fichier) != NULL){
        /* Si présence positionnement '#P' ou '#R' . */
        if((chaine[0] == '#' && chaine[1] == 'P') ||\
           (chaine[0] == '#' && chaine[1] == 'R')){
               posY(chaine, &py);
               posX(chaine, &px);
               pos = 1;
        }
        /* Si présence du '#r'. */
        if(chaine[0] == '#' && chaine[1] == 'r'){
            supprimeRetourLigne(chaine);
            p = strchr(chaine, ' ') + 1;
            if(strcmp(p, "23/3") && strcmp(p, "3/23")){
                strcat(texte, "   --->   Règle non gérée !");
                printf("Règle non gérée !\n");
                goto Erreur;
            }
        }
        /* Ligne d'en-tête. */
        if(chaine[0] == 'x' || chaine[0] == 'X'){
            delSpace(chaine);
            if(lignexyrRLE(chaine, &x, &y)){
                strcat(texte, "   --->   Règle non gérée !");
                goto Erreur;
            }
            break;
        }
    }

    if(!x && !y && !pos){
        printf("parse ligne x et y et positionnement vide.\n");
        strcat(texte, "   --->   ligne x et y non rempli !");
        goto Erreur;
    }

    unsigned int centre_i, centre_j, tmp_i, tmp_j;

    if(x > dim.largeur || y > dim.hauteur){
        printf("Pattern trop grand\n");
        strcat(texte, "   --->   Pattern trop grand !");
        goto Erreur;
    }

    centre_i = dim.hauteur / 2;
    centre_j = dim.largeur / 2;

    int ind = 0, n = 0;
    if(pos){
        centre_i += py;
        centre_j += px;
    }
    else{
        /* Arrondi au supérieur si décimal. */
        centre_i -= ((float) y / 2) + 0.5;
        centre_j -= ((float) x / 2) + 0.5;
    }

    tmp_i = centre_i;
    tmp_j = centre_j;

    do
    {
        chaine[ind] = fgetc(fichier);

        if(chaine[ind] == '!') break;

        //if(chaine[ind] == 'o'){
        if(!(chaine[ind] >= '0' && chaine[ind] <= '9') &&\
             chaine[ind] != 'b' && chaine[ind] != 'B' &&\
             chaine[ind] != '$' && chaine[ind] != '\r' && chaine[ind] != '\n'){

            chaine[++ind] = '\0';

            ind = 0;
            while(chaine[ind] != '\0')
            {
                if(chaine[ind] == 'b'){
                    if(!n)
                        n = 1;
                    tmp_j += n;
                    n = 0;
                }

                else if(chaine[ind] == '$'){
                    tmp_j = centre_j;
                    if(!n)
                        n = 1;
                    tmp_i += n;
                    n = 0;
                }

                else if(chaine[ind] >= '0' && chaine[ind] <= '9')
                    n = n * 10 + chaine[ind] - '0';

                ++ind;
            }

            ind = 0;
            do
            {
                set_cell(orig, dim, ++tmp_j, tmp_i);
                (orig[tmp_i][tmp_j]) |= 0x80;
                *nbVivantes+=1;
            } while (++ind < n);
            n = ind = 0;
            chaine[ind] = '\0';
        }
        else
            ++ind;
    } while (chaine[ind] != EOF);

    fclose(fichier);
    return 0;

Erreur:
    if(fichier != NULL) fclose(fichier);
    printf("Fonction: figureDansTableau, fichier: parse.c\n");
    return -1;
}

