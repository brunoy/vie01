#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <locale.h>
#include <libintl.h>

#ifdef _WIN32
    #include <fcntl.h>
#endif

#include "const.h"
#include "struc.h"
#include "textures.h"
#include "memoire.h"
#include "fichiers_recherche.h"
#include "fichiers_menu.h"
#include "fichiers_sauver.h"
#include "init.h"

void initLocales(void)
{
    char *TEXTDOMAINDIR = getenv("TEXTDOMAINDIR");

    if(TEXTDOMAINDIR == NULL || TEXTDOMAINDIR[0] == '\0')
        TEXTDOMAINDIR = LOCALEDIR;

    setlocale(LC_ALL, "");
    bindtextdomain(GETTEXT_PACKAGE, TEXTDOMAINDIR);
    bind_textdomain_codeset(GETTEXT_PACKAGE, "UTF-8");
    textdomain(GETTEXT_PACKAGE);
}

int initSDL(Fenetre *creer, Rendu *renderer)
{
    creer->fenetre = NULL;
    renderer->rendu = NULL;

    if(0 != SDL_Init(SDL_INIT_VIDEO))
    {
        fprintf(stderr, "Erreur SDL_Init : %s", SDL_GetError());
        return -1;
    }

    creer->fenetre = SDL_CreateWindow(" vie01", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                        LARGEUR_FENETRE, HAUTEUR_FENETRE, SDL_WINDOW_SHOWN);
    if(creer->fenetre == NULL)
    {
        fprintf(stderr, "Erreur SDL_CreateWindow : %s", SDL_GetError());
        return -1;
    }

    renderer->rendu = SDL_CreateRenderer(creer->fenetre, -1, SDL_RENDERER_ACCELERATED);
    if(renderer->rendu == NULL)
    {
        fprintf(stderr, "Erreur SDL_CreateRenderer : %s", SDL_GetError());
        return -1;
    }

    if(0 != TTF_Init())
    {
        fprintf(stderr, "Erreur initialisation TTF : %s", TTF_GetError());
        return -1;
    }

    /* Crée une icone sous linux. */
#ifdef __linux__
    SDL_Surface *icone = NULL;
    icone = image();
    SDL_SetWindowIcon(creer->fenetre, icone);
    if(NULL != icone) SDL_FreeSurface(icone);
#endif

    return 0;
}

int initBaseTexture(Rendu *base)
{
    /* Init position tableau de bord et barre du bas. */
    base->positionTabBord.x = LARGEUR_GRILLE;
    base->positionTabBord.y = 0;
    base->positionTabBord.w = LARGEUR_FENETRE - LARGEUR_GRILLE;
    base->positionTabBord.h = HAUTEUR_FENETRE-(HAUTEUR_FENETRE-HAUTEUR_GRILLE);

    base->positionBarre.x = 0;
    base->positionBarre.y = HAUTEUR_GRILLE;
    base->positionBarre.w = LARGEUR_FENETRE;
    base->positionBarre.h = HAUTEUR_FENETRE - HAUTEUR_GRILLE;

    /* Init des textures */
    base->tableauDeBord = base->barreInfo = base->bordH = base->bordG = NULL;

    base->tableauDeBord = constrTabDeBord(base->rendu);
    if(base->tableauDeBord == NULL) return -1;

    base->barreInfo = barre(base->rendu);
    if(base->barreInfo == NULL) return -1;

    base->bordH = bord(base->rendu, LARGEUR_GRILLE, BORDURE);
    if(base->bordH == NULL) return -1;

    base->bordG = bord(base->rendu, BORDURE, HAUTEUR_GRILLE - BORDURE);
    if(base->bordG == NULL) return -1;

    return 0;
}

int initBoutons(Boutons *bouton, SDL_Renderer *renderer)
{
    /* Init positions boutons rectangulaires */
    bouton->positionB[0].x = bouton->positionB[0].y = 0;
    bouton->positionB[0].w = LARG_BOUTON;
    bouton->positionB[0].h = HAUT_BOUTON;

    /* position boutons radio zoom (Normal par defaut)*/
    bouton->positionB[1].x = BOUTON_SELECZ_X + ((BOUTON_RADIO + ESP_RADIO_XB)*3);
    bouton->positionB[1].y = BOUTON_SELECZ_Y;
    bouton->positionB[1].w = bouton->positionB[1].h = BOUTON_SELEC;

    /* position boutons radio vitesse (par defaut)*/
    bouton->positionB[2].x = BOUTON_SELECV_X + ((BOUTON_RADIO + ESP_RADIO_XB)*3);
    bouton->positionB[2].y = BOUTON_SELECV_Y;
    bouton->positionB[2].w = bouton->positionB[2].h = BOUTON_SELEC;

    /* Init bouton radios */
    for(int i = 0; i < NB_RADIO; i++)
        bouton->selecRadioZ[i] = bouton->selecRadioV[i] = 0;

    /* Init en milli-secondes des boutons radio vitesse */
    bouton->selecRadioV[5] = 800; bouton->selecRadioV[6] = 300;
    bouton->selecRadioV[7] = 100; bouton->selecRadioV[8] = 50;
    bouton->selecRadioV[9] = 0;

    /* vitesse et zoom par défaut */
    bouton->selecRadioV[3] = 1;
    bouton->selecRadioZ[3] = 1;

    /* Init des textures */
    bouton->survol = bouton->clique = bouton->quitte = bouton->radioV = bouton->radioZ = NULL;

    bouton->survol = boutons(renderer, 0);
    if(bouton->survol == NULL) return -1;

    bouton->clique = boutons(renderer, 1);
    if(bouton->clique == NULL) return -1;

    bouton->quitte = quit(renderer);
    if(bouton->quitte == NULL) return -1;

    bouton->radioV = btRadio(renderer, 0);
    if(bouton->radioV == NULL) return -1;

    bouton->radioZ = btRadio(renderer, 1);
    if(bouton->radioZ == NULL) return -1;

    return 0;
}

int initGrilles(Grilles *damier, SDL_Renderer *renderer)
{
    damier->pixel1 = damier->pixel2 = damier->pixel3 = damier->pixel6 = damier->pixel9 = NULL;

    /* Init position de la grille */
    damier->positionG.x = 0;
    damier->positionG.y = 0;
    damier->positionG.w = LARGEUR_GRILLE;
    damier->positionG.h = HAUTEUR_GRILLE;

    /* Init et vérif des textures */
    damier->pixel1 = grilles(renderer, CELLULE);
    if(damier->pixel1 == NULL) return -1;

    damier->pixel2 = grilles(renderer, CELLULE_Z2);
    if(damier->pixel2 == NULL) return -1;

    damier->pixel3 = grilles(renderer, CELLULE_Z3);
    if(damier->pixel3 == NULL) return -1;

    damier->pixel6 = grilles(renderer, CELLULE_Z6);
    if(damier->pixel6 == NULL) return -1;

    damier->pixel9 = grilles(renderer, CELLULE_Z9);
    if(damier->pixel9 == NULL) return -1;

    return 0;
}

int initIterations(Iterations *iter, int vit, SDL_Renderer *renderer)
{   /* Init structure Iteration */

    iter->car[0] = '\0';

    /* Init texture */
    iter->chiffres = NULL;
    iter->chiffres = genChiffre(renderer);
    if(iter->chiffres == NULL) return -1;

    /* Init taille et position src et dst pour l'affichage dynamique des itérations */
    iter->src.x = iter->src.y = iter->src.w = iter->src.h = 0;

    /* Récupère la taille en w et h de la texture chiffres*/
    if(SDL_QueryTexture(iter->chiffres, NULL, NULL, &iter->src.w, &iter->src.h) < 0)
    {
        fprintf(stderr, "Erreur QueryTexture chiffres itération init.c: %s", SDL_GetError());
        return -1;
    }

    iter->dst.x = CADRE_X + 15; iter->dst.y = CADREG_Y + 10;   /* À gauche */
    /* divise par 10 pour avoir la largeur d'un chiffre seul. */
    iter->dst.w = iter->src.w / 10; iter->dst.h = iter->src.h;
    iter->src.w = iter->dst.w;

    iter->compteur = 0;
    iter->delai = vit; /* Vitesse sélectionnée par défaut */

    return 0;
}

int initCellules(Cellules *cellule, SDL_Renderer *renderer)
{
    cellule->vivante = NULL;
    cellule->vivante = cellules(renderer);
    if(cellule->vivante == NULL) return -1;

    cellule->position.x = cellule->position.y = 0;
    cellule->position.w = cellule->position.h = CELLULE_Z6 - 1;

    cellule->nbVivantes = 0;
    cellule->type = CELLULE_Z6;

    return 0;
}

int initTableaux(Tableaux *tab)
{
    /* Déclaration */
    tab->map = NULL;

    /* Allocation mémoire */
    if(0 != allocMem(&tab->map)) return -1;

    /* Init du tableau à 0. */
    memset(&tab->map[0][0], 0, LARGEUR_GRILLE * HAUTEUR_GRILLE * sizeof tab->map[0][0]);

    return 0;
}

void initDimTableaux(DimTableaux *dim)
{
    dim->largeur = LARGEUR_GRILLE / CELLULE;
    dim->hauteur = HAUTEUR_GRILLE / CELLULE;
    dim->debMargeL = (dim->largeur - (LARGEUR_GRILLE/CELLULE_Z6)) /2;
    dim->finMargeL = dim->largeur - dim->debMargeL;
    dim->debMargeH = (dim->hauteur - (HAUTEUR_GRILLE/CELLULE_Z6)) /2;
    dim->finMargeH = dim->hauteur- dim->debMargeH;

    return;
}

int initSauver(Sauvegarder *svg)
{
    svg->pattHome = NULL;
    svg->srcT.x = svg->srcT.y = svg->srcT.w = svg->srcT.h = 0;
    svg->dstT.x = ZONE_X; svg->dstT.y = ZONE_Y; svg->dstT.w = svg->dstT.h = 0;

    svg->zoneMess[0] = ' ';
    svg->zoneMess[1] = '\0';
    svg->zoneText[0] = '|';
    svg->zoneText[1] = '\0';

    return 0;
}

int initRepertLinux(char **pattHome)
{
    const char *home = getenv("HOME");
    const char *prog = "/.vie01";
    const char *rept = "/patterns";
    const char *user = DOSPERSO;

    *pattHome = malloc( (strlen(home) + strlen(prog) + strlen(rept) + 1)\
                            * sizeof (char) );
    if(*pattHome == NULL) { printf("Erreur malloc init\n"); return -1; }

    strcpy(*pattHome, home);
    strcat(*pattHome, prog);

    if(!dir(*pattHome)){
        fprintf(stderr, "Création dossier caché echouée. Fichier avec même nom. À corriger");
        return -1;
    }
    else if(0 > dir(*pattHome)){
        mkdir(*pattHome, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    }

    strcat(*pattHome, rept);

    if(!dir(*pattHome)){
        fprintf(stderr, "Création dossier 'patterns' echouée. Fichier avec même nom. À corriger");
        return -1;
    }
    else if(0 > dir(*pattHome)){
        mkdir(*pattHome, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    }

    char *monPatt = malloc(strlen(*pattHome) + strlen(user) + 1 * sizeof (char) );
    if(monPatt == NULL) { printf("Erreur malloc init\n"); return -1; }

    strcpy(monPatt, *pattHome);
    strcat(monPatt, user);

    if(!dir(monPatt)){
        fprintf(stderr, "Création dossier 'auser' echouée. Fichier avec même nom. À corriger");
        return -1;
    }
    else if(0 > dir(monPatt)){
        mkdir(monPatt, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    }

    free(monPatt);
    return 0;

}

int initMenu(MenuDyn *menu, const char *monRep)
{
    /* Initialise le tableau */
    menu->cheminFichier = NULL;

    /* Récupère nombre de fichiers */
    menu->nbFichiers = compterFichierLifRle(USRPATTERNS);
    menu->nbFichiers += compterFichierLifRle(monRep);
    //printf("%s\n", monRep);

    /* Si pas de fichier, quitte. */
    if(!menu->nbFichiers)
        return 0;

    /* Si supérieur au max fichiers autorisés */
    if(menu->nbFichiers > MAX_FICHIERS)
        menu->nbFichiers = MAX_FICHIERS;

    /* Alloue mémoire du tableau */
    if(allocMemChaine(&menu->cheminFichier, menu->nbFichiers)) return -1;

    for (int i = 0; i < menu->nbFichiers; i++)
    {
        menu->cheminFichier[i] = NULL;
    }

    /* Remplissage tableau des noms fichiers et leur chemin */
    int index = 0;

    index = ParcourirFichiers(monRep, menu->cheminFichier, &index);
    ParcourirFichiers(USRPATTERNS, menu->cheminFichier, &index);


    /* Tri par dossier puis par ordre alphabétique. */
    qsort (menu->cheminFichier, menu->nbFichiers, sizeof *menu->cheminFichier, compare);

    /* Détermine hauteur et largeur du menu */
    tailleCaseMenu(menu->cheminFichier, &menu->menuW, &menu->menuH, menu->nbFichiers);

    /* Couleur */
    //128, 139, 150, 255
    menu->fg.r = 33; menu->fg.g = 47; menu->fg.b = 61;
    menu->bg.r = 242; menu->bg.g = 243; menu->bg.b = 244;
    menu->fg.a = menu->bg.a = 255;

    int nombreCaseMenu = (menu->nbFichiers > ELEMENTS_MAX) ? ELEMENTS_MAX : menu->nbFichiers;

    /* Valeur parcours tableau */
    menu->deb = 0;
    menu->fin = nombreCaseMenu;
    menu->cases = 0;

    /* Font menu */
    menu->fondM.x = POSX_MENU(menu->menuW) - MARGE;
    menu->fondM.y = POSY_MENU;
    menu->fondM.w = menu->menuW;
    menu->fondM.h = menu->menuH * nombreCaseMenu;
    /* Taille et position menu */
    menu->posM.x = POSX_MENU(menu->menuW);
    menu->posM.y = POSY_MENU;
    menu->posM.w = menu->menuW;
    menu->posM.h = menu->menuH;
    /* Menu survol */
    menu->survolM.x = POSX_MENU(menu->menuW) - MARGE;
    menu->survolM.y = POSY_MENU;
    menu->survolM.w = menu->menuW;
    menu->survolM.h = menu->menuH;
    return 0;
}

SDL_Surface *image(void)
{
    SDL_Surface *surface = SDL_CreateRGBSurface(0, 32, 32, 32, 0, 0, 0, 0);

    struct carre {
        SDL_Rect rect;
        Uint32 couleur;
    };

    /* On crée neuf carrés pour notre icône. */
    struct carre carre[9] = {
        { { 2,  2, 8, 8 }, SDL_MapRGB(surface->format, 85, 85, 85) },
        { { 2, 12, 8, 8 }, SDL_MapRGB(surface->format, 85, 85, 85) },
        { { 2, 22, 8, 8 }, SDL_MapRGB(surface->format, 214, 219, 223) },
        { { 12,  2, 8, 8 }, SDL_MapRGB(surface->format, 214, 219, 223) },
        { { 12, 12, 8, 8 }, SDL_MapRGB(surface->format, 214, 219, 223) },
        { { 12, 22, 8, 8 }, SDL_MapRGB(surface->format, 85, 85, 85) },
        { { 22,  2, 8, 8 }, SDL_MapRGB(surface->format, 85, 85, 85) },
        { { 22, 12, 8, 8 }, SDL_MapRGB(surface->format, 214, 219, 223) },
        { { 22, 22, 8, 8 }, SDL_MapRGB(surface->format, 214, 219, 223) }
    };

    for(int i = 0; i < 9; i++)
        SDL_FillRect(surface, &carre[i].rect, carre[i].couleur);

    return surface;
}
