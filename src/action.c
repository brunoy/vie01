#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include <locale.h>
#include <libintl.h>
#include "const.h"
#include "struc.h"
#include "textures.h"
#include "generation.h"
#include "fichiers_sauver.h"


void boutonAfficher(char *generer, char *etat_menu, char *etat_survol)
{
    if(*generer)
        *generer = 0;
    if(!*etat_menu){
        *etat_menu = 1;
        *etat_survol = 0;
    }
}

void boutonAleatoire(unsigned char **map, DimTableaux dim, int *cpt, int *nbVivantes)
{
    *cpt = 0;
    genererCellulesAleatoires(map, dim, nbVivantes);
}

void boutonStopOuUn(unsigned char **map, DimTableaux dim, Cellules *cel,
                             int *cpt, char *memo, SDL_Renderer *ren)
{
    if(*memo){
        *memo = 0;
        copieMemo(map, dim);
    }

    if(!tableauVide(map, dim, 0x01)){
        genererNextCellules(map, dim, cel, ren);
        *cpt+=1;
    }
}

void boutonDernierJeu(unsigned char **map, DimTableaux dim, int *cpt, int *vivantes)
{
    if(!tableauVide(map, dim, (1<<6))){
        *cpt = 0;
        initTab(map, dim);
        lireMemo(map, dim, vivantes);
    }
}

int boutonLancer(unsigned char **map, DimTableaux dim, char *memo)
{
    if(tableauVide(map, dim, 0x01))
        return 1;

    if(*memo){
        *memo = 0;
        copieMemo(map, dim);
    }
    return 0;
}

void boutonEffacer(unsigned char **map, DimTableaux dim, char *etat_aide,
                   char *generer, int *nbGenerations, int *nbVivantes)
{
    if(*etat_aide) *etat_aide = 0;
    if(*generer) *generer = 0;
    if(*nbGenerations) *nbGenerations = 0;
    if(*nbVivantes) *nbVivantes = 0;

    initTab(map, dim);
}

void boutonAide(SDL_Texture **aide, SDL_Renderer *ren, char *etat_aide)
{
    if(!*etat_aide){
        if(*aide == NULL){
            *aide = aideTex(ren);
            if(*aide == NULL){
                printf("Erreur texture aide demarrer.c\n");
                return;
            }
        }
        *etat_aide = 1;
    }
    else{
        *etat_aide = 0;
    }
}

int boutonSauvegarder(DimTableaux dim, MenuDyn *menu, unsigned char **map, char *monRep,
                      char *nom, char *mess)
{
    char *monPattern = NULL;
    int statut = 1;

    if(tableauVide(map, dim, 0x01)){
        strcpy(mess, _("Carte vide"));
        goto Quit;
    }

    if(strlen(nom) > 1)
        nom[strlen(nom)-1] = '\0'; /* Supprime le curseur barre vertical. */
    else{
        strcpy(mess, _("Vous n'avez rien saisi"));
        goto Quit;
    }

    monPattern = concat(nom, monRep);

    if(monPattern == NULL){
        strcpy(mess, _("Erreur concat nom"));
        goto Quit;
    }

    if(verifNomPresent(menu->cheminFichier, monPattern, menu->nbFichiers) > 0){
        strcpy(mess, _("Nom déjà présent !"));
        strcat(nom, "|");
        goto Quit;
    }
    else if(verifNomPresent(menu->cheminFichier, monPattern, menu->nbFichiers) < 0){
        strcpy(mess, _("Erreur verif si nom présent !"));
        strcat(nom, "|");
        goto Quit;
    }

    if(dir(monRep) < 0){
        strcpy(mess, _("Pas de dossier perso !"));
        goto Quit;
    }

    if(!ecrireFichieRLE(dim, map, nom, monPattern)){
        ajoutPatternBase(menu, monPattern);
    }

    initChaine(nom);
    initMessage(mess);

    statut = 0;
Quit:
    if(monPattern != NULL)
        free(monPattern);
    return statut;
}

int boutonRadioVitesse(Boutons *bouton, int *delai, int *tempo, const int n)
{
    if(bouton->selecRadioV[n])
        return 0;

    int i = 0;
    while(!bouton->selecRadioV[i])
        i++;

    bouton->selecRadioV[i] = 0;
    bouton->selecRadioV[n] = 1;

    bouton->positionB[2].x = BOUTON_SELECV_X + (n*(BOUTON_RADIO+ESP_RADIO_XB));
    *delai = bouton->selecRadioV[n+NB_RADIO];

    if(*delai)
        *tempo = 50; /* Temporisation boucle principale */
    else
        *tempo = 0; /* " */

    return 1;
}

int vitesseClavier(Boutons *bouton, int *delai, int *tempo, const int ordre)
{
    int i = 0;

    while(!bouton->selecRadioV[i])
        i++;

    if(ordre > 0 && i == NB_RADIO-1)
        return 0;
    if(ordre < 0 && i == 0)
        return 0;

    bouton->selecRadioV[i] = 0;
    bouton->selecRadioV[i+=ordre] = 1;

    bouton->positionB[2].x = BOUTON_SELECV_X + (i*(BOUTON_RADIO+ESP_RADIO_XB));
    *delai = bouton->selecRadioV[i+NB_RADIO];

    if(*delai)
        *tempo = 50; /* Temporisation boucle principale */
    else
        *tempo = 0; /* " */

    return 1;
}

int boutonRadioZoom(Boutons *bouton, int *zoom, const int n)
{
    if(bouton->selecRadioZ[n])
        return 0;

    int i = 0;
    while(!bouton->selecRadioZ[i])
        i++;

    bouton->selecRadioZ[i] = 0;
    bouton->selecRadioZ[n] = 1;

    switch(n){
        case 0: *zoom = CELLULE; break;
        case 1: *zoom = CELLULE_Z2; break;
        case 2: *zoom = CELLULE_Z3; break;
        case 3: *zoom = CELLULE_Z6; break;
        case 4: *zoom = CELLULE_Z9; break;
    }

    bouton->positionB[1].x = BOUTON_SELECZ_X + (n*(BOUTON_RADIO+ESP_RADIO_XB));
    return 1;
}

int zoomClavier(Boutons *bouton, int *zoom, const int ordre)
{
    int i = 0;

    while(!bouton->selecRadioZ[i])
        i++;

    if(ordre > 0 && i == NB_RADIO-1)
        return 0;
    if(ordre < 0 && i == 0)
        return 0;

    bouton->selecRadioZ[i] = 0;
    bouton->selecRadioZ[i+=ordre] = 1;

    switch(i){
        case 0: *zoom = CELLULE; break;
        case 1: *zoom = CELLULE_Z2; break;
        case 2: *zoom = CELLULE_Z3; break;
        case 3: *zoom = CELLULE_Z6; break;
        case 4: *zoom = CELLULE_Z9; break;
    }

    bouton->positionB[1].x = BOUTON_SELECZ_X + (i*(BOUTON_RADIO+ESP_RADIO_XB));
    return 1;
}

void activeCellule(unsigned char **map, DimTableaux dim, int x, int y, int *nbVivantes)
{
    set_cell(map, dim, x, y);
    map[y][x] |= 0x80;
    *nbVivantes+=1;
}

void desactiveCellule(unsigned char **map, DimTableaux dim, int x, int y, int *nbVivantes)
{
    clear_cell(map, dim, x, y);
    map[y][x] &= ~0x80;
    *nbVivantes-=1;
}
