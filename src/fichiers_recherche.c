#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include "const.h"
#include "fichiers_recherche.h"

/*** //////////////////////////////////////////////////////////////////////////////////////
 *                    Partie qui recherche les fichiers.lif s'il y a,                     *
 *                    dans le dossier 'patterns' récursivement.                           *
 * /////////////////////////////////////////////////////////////////////////////////// ***/


char isDir(const char *name)
{
    struct stat stbuf;
    if (stat(name, &stbuf) == -1) {
        fprintf(stderr, "fsize: can't access %s\n", name);
        return 0;
    }
    return ((stbuf.st_mode & S_IFMT) == S_IFDIR);
}

int verifExtension(const char * fileExt, const char * extension)
{ // L'extension est ici à fournir sans le . (point) !

    if(*fileExt == '\0' || *fileExt == '.')
        return 1;

    while (*fileExt != '\0' && toupper(*fileExt) == *extension)
    {
        fileExt++;
        extension++;
    }
    return (*(unsigned char *) fileExt) - (*(unsigned char *) extension);

}

int compterFichierLifRle(const char *dossier)
{
    int nombre = 0;

    DIR *rep = NULL;
    rep = opendir(dossier);
    if(rep == NULL){
        perror(dossier);
        goto Quit;
    }

    int longueur = strlen(dossier)+2;
    if(longueur > LIMIT){
        goto Quit;
    }

    struct dirent *lecture;

    char chemin[LIMIT] = "";
    char copy[LIMIT] = "";
    strcpy(chemin, dossier);
    strcat(chemin, "/");

    char *p = NULL;

    while ((lecture = readdir(rep)) != NULL)
    {
        if(longueur + strlen(lecture->d_name) > LIMIT){
            continue;
        }

        p = strrchr (lecture->d_name, '.');

        if(p != NULL){
            ++p;
            if(!verifExtension(p, "LIF") || !verifExtension(p, "RLE")){
                nombre += 1;
            }
            p = NULL;
        }
        else{
            strcpy(copy, chemin);
            if(isDir(strcat(copy, lecture->d_name)))
                nombre += compterFichierLifRle(copy);
        }
    }
Quit:
    closedir(rep);
    return nombre;
}

int ParcourirFichiers(const char *dossier, char **tabChaine, int *index)
{
    DIR *rep = opendir(dossier);
    if(rep == NULL){
        perror(dossier);
        goto Quit;
    }

    int longueur = strlen(dossier)+2;
    if(longueur > LIMIT){
        printf("Longueur '%s/' supérieur à la limite (%d)\n", dossier, LIMIT);
        goto Quit;
    }

    struct dirent *lecture;

    char chemin[LIMIT] = "", copy[LIMIT] = "";

    strcat(chemin, dossier);
    strcat(chemin, "/");

    char *p = NULL;

    while ((lecture = readdir(rep)) != NULL)
    {
        if(longueur + strlen(lecture->d_name) > LIMIT){
            printf("Longueur %s%s supérieur à la limite (%d)\n", dossier, lecture->d_name, LIMIT);
            continue;
        }

        p = strrchr (lecture->d_name, '.');

        if(p != NULL){
            ++p;
            if(!verifExtension(p, "LIF") || !verifExtension(p, "RLE")){
                strcpy(copy, chemin);
                tabChaine[*index] = strdup(strcat(copy, lecture->d_name));

                if(NULL == tabChaine[*index]){
                    libereMem(tabChaine, "erreur strdup", *index);
                    return -1;
                }

                *index += 1;
                if(*index >= MAX_FICHIERS) goto Quit;
            }
            p = NULL;
        }
        else{
            strcpy(copy, chemin);
            if(isDir(strcat(copy, lecture->d_name)))
                ParcourirFichiers(copy, tabChaine, index);
        }
    }
Quit:
    closedir(rep);

    return *index;
}

int indiceNomMaxi(char **chaine, const int ligne)
{
    int i, indice = 0, max = 0, longueur_chaine;

    for (i = 0; i < ligne; i++)
    {
        longueur_chaine = strlen(chaine[i]);
        if(max < longueur_chaine){
            max = longueur_chaine;
            indice = i;
        }
    }
    return indice;
}

char *nomSansChemin(const char *chaine)
{
    char *copie = NULL, *p = strrchr(chaine, '/') + 1;

    copie = malloc((strlen(p) + 1) * sizeof(char));
    strcpy(copie, p);
    return copie;
}

char *nomSansExtension(const char *chaine)
{
    char *copie = NULL, *p = strrchr(chaine, '/') + 1;

    copie = malloc(strlen(p) * sizeof(char));

    for(int i = 0; p[i] != '\0'; i++){
        if(p[i] == '.'){
            copie[i] = '\0';
            break;
        }
        copie[i] = p[i];
    }
    return copie;
}

void libereMem(char **tableau, char *chaine, int indice)
{
    if(NULL != chaine) perror(chaine);
    for(int i = 0; i < indice; i++){
        free(tableau[i]);
        tableau[i] = NULL;
    }
    free(tableau);
    tableau = NULL;
}

/* fonction utilisateur de comparaison fournie a qsort() */
int compare (void const *a, void const *b)
{
   /* definir des pointeurs type's et initialise's avec les paramètres */
   char const *const *pa = a;
   char const *const *pb = b;

   /* evaluer et retourner l'etat de l'evaluation (tri croissant) */
   return strcmp (*pa, *pb);
}
