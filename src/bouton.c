#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <locale.h>
#include <libintl.h>
#include "const.h"
#include "textures.h"
#include "bouton.h"


/* Dessine le logo Quitter */
int logoBoutonQuit(SDL_Renderer *renderer, const int x_centre, const int y_centre, const int rayon)
{
    /* Algorithme de tracé d'arc de cercle de Bresenham.\
     * Ne trace pas les 2 arcs en haut du cercle (commentés)... */
    int m, x, y;
    x = 0;
    y = rayon;
    m = 5 - (4*rayon);
    while(x <= y){
        if(0 != SDL_RenderDrawPoint(renderer, x + x_centre, y + y_centre)) goto Erreur;
        if(0 != SDL_RenderDrawPoint(renderer, y + x_centre, x + y_centre)) goto Erreur;
        if(0 != SDL_RenderDrawPoint(renderer, -x + x_centre, y + y_centre)) goto Erreur;
        if(0 != SDL_RenderDrawPoint(renderer, -y + x_centre, x + y_centre)) goto Erreur;
        //SDL_RenderDrawPoint(renderer, x + x_centre, -y + y_centre);
        if(0 != SDL_RenderDrawPoint(renderer, y + x_centre, -x + y_centre)) goto Erreur;
        //SDL_RenderDrawPoint(renderer, -x + x_centre, -y + y_centre);
        if(0 != SDL_RenderDrawPoint(renderer, -y + x_centre, -x + y_centre)) goto Erreur;

        if(m > 0){
            y--;
            m = m-(8*y);
        }
        x++;
        m = m + (8*x+4);
    }

    /* ... et ici on ajoute une petite barre verticale */
    if(0 != SDL_RenderDrawLine(renderer,x_centre,y_centre-rayon,x_centre,y_centre-1)) goto Erreur;
    return 0;
Erreur:
    printf("Erreur SDL Fc logoBoutonQuit, fichier bouton.c : %s\n", SDL_GetError());
    return -1;
}

int cadreBouton(SDL_Renderer *renderer)
{
    /* Dimensions et position premier cadre */
    SDL_Rect rect = {CADRE_X - LARGEUR_GRILLE + 7, CADREG_Y + 5, LARG_CADRE - 10, HAUT_CADRE_A - 10};

    /* Couleur */
    if(SDL_SetRenderDrawColor(renderer, 213, 216, 220, 255) < 0) goto Erreur;

    /* Cadre nombre de générations */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    /* Cadre nombre de cellules vivantes */
    rect.y = CADREV_Y+5; //rect.h = HAUT_CADRE_A;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE2_Y; rect.h = HAUT_CADRE_B; rect.x = CADRE_X - LARGEUR_GRILLE; rect.w = LARG_CADRE;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE3_Y;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE4_Y; rect.h = HAUT_CADRE_C;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE5_Y;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE6_Y; rect.h = HAUT_CADRE_B;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE7_Y;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, Fonction cadreBouton, fichier bouton.c : %s\n", SDL_GetError());
    return -1;
}

int boutonsNormaux(SDL_Renderer *renderer)
{
    /* Couleurs */
    if(SDL_SetRenderDrawColor(renderer, 128, 139, 150, 255) < 0) goto Erreur;

    /* Position */
    SDL_Rect pos = {BOUTON_GX - LARGEUR_GRILLE, CADRE2_Y + ESP_BOUTON_Y, LARG_BOUTON, HAUT_BOUTON};
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;

    pos.x = BOUTON_DX - LARGEUR_GRILLE;
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;

    pos.y = CADRE3_Y + ESP_BOUTON_Y;
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;

    pos.x = BOUTON_GX - LARGEUR_GRILLE;
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;

    pos.y = CADRE6_Y + ESP_BOUTON_Y;
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;
    pos.x = BOUTON_DX - LARGEUR_GRILLE;
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;

    pos.y = CADRE7_Y + ESP_BOUTON_Y;
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;
    pos.x = BOUTON_GX - LARGEUR_GRILLE;
    if(SDL_RenderFillRect(renderer, &pos) < 0) goto Erreur;

    /* Logo quitter */
    pos.x = BOUTON_DX - LARGEUR_GRILLE;
    if(SDL_SetRenderDrawColor(renderer, 194, 202, 211, 255) < 0) goto Erreur;
    logoBoutonQuit(renderer,pos.x+(pos.w/2), pos.y+(pos.h/2), 5);

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, Fonction boutonNormaux, fichier bouton.c : %s\n", SDL_GetError());
    return -1;
}

int coloriseTitreCadre(SDL_Renderer *renderer)
{
    SDL_Rect rect = {CADRE_X - LARGEUR_GRILLE, CADRE2_Y - HAUT_CADRE_CL - ESP_CL,
                     LARG_CADRE_CL, HAUT_CADRE_CL};

    if(SDL_SetRenderDrawColor(renderer, 252, 243, 207, 255) < 0) goto Erreur; /* jaune */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE3_Y - HAUT_CADRE_CL - ESP_CL;
    if(SDL_SetRenderDrawColor(renderer, 244, 236, 247, 255) < 0) goto Erreur; /* Violet pale */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE4_Y - HAUT_CADRE_CL - ESP_CL;
    if(SDL_SetRenderDrawColor(renderer, 250, 229, 211, 255) < 0) goto Erreur; /* Marron */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE5_Y - HAUT_CADRE_CL - ESP_CL;
    if(SDL_SetRenderDrawColor(renderer, 224, 238, 248, 255) < 0) goto Erreur; /* Bleu */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE6_Y - HAUT_CADRE_CL - ESP_CL;
    if(SDL_SetRenderDrawColor(renderer, 218, 250, 232, 255) < 0) goto Erreur; /* Vert */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    rect.y = CADRE7_Y - HAUT_CADRE_CL - ESP_CL;
    if(SDL_SetRenderDrawColor(renderer, 253, 237, 236, 255) < 0) goto Erreur; /* Rose */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    /* Cadre coloré qui contiendra le nombre de générations */
    rect.x = X_NOMBRE_GEN_PT - LARGEUR_GRILLE;
    rect.y = Y_NOMBRE_GEN_PT;
    rect.w = X_NOMBRE_GEN_LG;
    rect.h = Y_NOMBRE_GEN_LG;
    if(SDL_SetRenderDrawColor(renderer, 93, 109, 126, 255) < 0) goto Erreur; /* noir bleuté */
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    /* Cadre coloré qui contiendra le nombre de cellules vivantes */
    rect.y = Y_NOMBRE_VIV_PT;
    if(SDL_RenderFillRect(renderer,&rect) < 0) goto Erreur;

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, Fct coloriseTitreCadre, fichier bouton.c : %s\n", SDL_GetError());
    return -1;
}

int texteFenetre(SDL_Renderer *rendu)
{
    TTF_Font * font = NULL;
    SDL_Color cl = {33, 47, 61, 255};

    /* Textes boutons */
    font = TTF_OpenFont(CHEMIN_FONT, 11);
    if(NULL == font) goto Erreur;

    int x = CADRE_X - LARGEUR_GRILLE;

    ecrireTexte(x + 12, CADREG_Y - 8, rendu, cl, font, _("Générations"));

    ecrireTexte(x + 12, CADREV_Y - 8, rendu, cl, font, _("Cellules vivantes"));

    ecrireTexte(x + 4, CADRE2_Y - 16, rendu, cl, font, _("Patterns  /  Sauver"));

    ecrireTexte(x + 4, CADRE3_Y - 16, rendu, cl, font, _("Aléatoire / Dernier joué"));

    ecrireTexte(x + 4, CADRE4_Y - 16, rendu, cl, font, _("-        Zoom        +"));

    ecrireTexte(x + 4, CADRE5_Y - 16, rendu, cl, font, _("-       Vitesse       +"));

    ecrireTexte(x + 4, CADRE6_Y - 16, rendu, cl, font, _("Stop|1  /  Lancer"));

    ecrireTexte(x + 4, CADRE7_Y - 16, rendu, cl, font, _("Effacer  /  Quitter"));

    TTF_CloseFont(font);

    font = TTF_OpenFont(CHEMIN_FONT, 15);
    if(NULL == font) goto Erreur;

    ecrireTexte(LARGEUR_FENETRE - 15, HAUTEUR_FENETRE - 19, rendu, cl, font, "?");

    TTF_CloseFont(font);
    return 0;
Erreur:
    if(font != NULL) TTF_CloseFont(font);
    fprintf(stderr, "Erreur SDL, Fct texteFenetre, fichier bouton.c : %s\n", SDL_GetError());
    return -1;
}

int texteBarre(SDL_Renderer *rendu)
{
    TTF_Font * font = NULL;
    SDL_Color cl = {33, 47, 61, 255};

    /* Textes barre */
    font = TTF_OpenFont(CHEMIN_FONT, 11);
    if(NULL == font) goto Erreur;

    ecrireTexte(10, 8, rendu, cl, font, "vie01");

    TTF_CloseFont(font);

    font = NULL;
    font = TTF_OpenFont(CHEMIN_FONT, 15);
    if(NULL == font) goto Erreur;

    ecrireTexte(LARGEUR_FENETRE - 15, 5, rendu, cl, font, "?");

    TTF_CloseFont(font);
    return 0;
Erreur:
    if(font != NULL) TTF_CloseFont(font);
    fprintf(stderr, "Erreur SDL, Fct texteBarre, fichier bouton.c : %s\n", SDL_GetError());
    return -1;
}
