#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdlib.h>
#include <time.h>
#include "struc.h"
#include "init.h"
#include "demarrer.h"


void free2D(unsigned char ***ptr);
void free2D2(char ***ptr, int taille);
void quitter(Programme *jeu);

int main(int argc, char *argv[])
{
    /* Déclarations structures. */
    Fenetre fenetre;
    Rendu base;
    Grilles grille;
    Boutons bouton;
    Cellules cellule;
    Iterations iter;
    DimTableaux dim;
    Tableaux tab;
    MenuDyn menu;
    Sauvegarder svg;

    int statut = EXIT_FAILURE;

    /* Init srand. */
    srand(time(NULL));

    /* Init locales. */
    initLocales();

    /* Initialisations sdl et structures */
    if(0 != initSDL(&fenetre, &base)) goto Quitter;
    if(0 != initBaseTexture(&base)) goto Quitter;
    if(0 != initBoutons(&bouton, base.rendu)) goto Quitter;
    if(0 != initGrilles(&grille, base.rendu)) goto Quitter;
    if(0 != initIterations(&iter, bouton.selecRadioV[8], base.rendu)) goto Quitter;
    if(0 != initCellules(&cellule, base.rendu)) goto Quitter;
    if(0 != initTableaux(&tab)) goto FreeQuitter;
    if(0 != initSauver(&svg)) goto FreeQuitter;
    if(0 != initRepertLinux(&svg.pattHome)) goto FreeQuitter;
    if(0 != initMenu(&menu, svg.pattHome)) goto FreeQuitter;
    initDimTableaux(&dim);

    /* Contient les principales structures. */
    Programme jeu = {fenetre, base, grille, bouton, cellule, iter, &menu, svg};

    /* Lance le jeu. */
    lancerLeJeu(jeu, dim, tab);

    /* unused. */
    (void) argc;
    (void) argv;

    /* Libére. */
    statut = EXIT_SUCCESS;

FreeQuitter:
    free2D(&tab.map);
    free2D2(&menu.cheminFichier, menu.nbFichiers);

Quitter:
    quitter(&jeu);

    return statut;
}

void free2D(unsigned char ***ptr)
{
    if(NULL != *ptr){
        free((*ptr)[0]);
        free((*ptr));
        *ptr = NULL;
    }
}

void free2D2(char ***ptr, int taille)
{
    if(*ptr != NULL){
        for (int i = 0; i < taille; i++){
            free((*ptr)[i]);
            (*ptr)[i] = NULL;
        }
        free((*ptr));
        *ptr = NULL;
    }
}

void quitter(Programme *jeu)
{
    /* Iteration */
    if(NULL != jeu->iter.chiffres)
        SDL_DestroyTexture(jeu->iter.chiffres);
    /* Cellules */
    if(NULL != jeu->cellule.vivante)
        SDL_DestroyTexture(jeu->cellule.vivante);
    /* Boutons*/
    if(NULL != jeu->bouton.survol)
        SDL_DestroyTexture(jeu->bouton.survol);
    if(NULL != jeu->bouton.clique)
        SDL_DestroyTexture(jeu->bouton.clique);
    if(NULL != jeu->bouton.quitte)
        SDL_DestroyTexture(jeu->bouton.quitte);
    if(NULL != jeu->bouton.radioV)
        SDL_DestroyTexture(jeu->bouton.radioV);
    if(NULL != jeu->bouton.radioZ)
        SDL_DestroyTexture(jeu->bouton.radioZ);
    /* Grilles */
    if(NULL != jeu->grille.pixel9)
        SDL_DestroyTexture(jeu->grille.pixel9);
    if(NULL != jeu->grille.pixel6)
        SDL_DestroyTexture(jeu->grille.pixel6);
    if(NULL != jeu->grille.pixel3)
        SDL_DestroyTexture(jeu->grille.pixel3);
    if(NULL != jeu->grille.pixel2)
        SDL_DestroyTexture(jeu->grille.pixel2);
    if(NULL != jeu->grille.pixel1)
        SDL_DestroyTexture(jeu->grille.pixel1);
    /* base fenetre */
    if(NULL != jeu->base.bordG)
        SDL_DestroyTexture(jeu->base.bordG);
    if(NULL != jeu->base.bordH)
        SDL_DestroyTexture(jeu->base.bordH);
    if(NULL != jeu->base.barreInfo)
        SDL_DestroyTexture(jeu->base.barreInfo);
    if(NULL != jeu->base.tableauDeBord)
        SDL_DestroyTexture(jeu->base.tableauDeBord);
    /* Renderer, window */
    if(NULL != jeu->base.rendu)
        SDL_DestroyRenderer(jeu->base.rendu);
    if(NULL != jeu->sdl.fenetre)
        SDL_DestroyWindow(jeu->sdl.fenetre);
    /* Sdl */
    TTF_Quit();
    SDL_Quit();
}
