#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include "const.h"
#include "struc.h"
//#include "fichiers_menu.h"
#include "affichage.h"



//void affichage(SDL_Texture *grille, Rendu *base, Iterations *iter)
int afficherBaseFenetre(Rendu base, Grilles grille, const int zoom)
{
    if(0 != SDL_RenderClear(base.rendu))
        goto Erreur;

    if(0 != SDL_RenderCopy(base.rendu, base.tableauDeBord, NULL, &base.positionTabBord))
        goto Erreur;

    if(0 != SDL_RenderCopy(base.rendu, base.barreInfo, NULL, &base.positionBarre))
        goto Erreur;

    switch(zoom)
    {
        case CELLULE:
                if(0 != SDL_RenderCopy(base.rendu, grille.pixel1, NULL, &grille.positionG))
                    goto Erreur;
                break;
        case CELLULE_Z2:
                if(0 != SDL_RenderCopy(base.rendu, grille.pixel2, NULL, &grille.positionG))
                    goto Erreur;
                break;
        case CELLULE_Z3:
                if(0 != SDL_RenderCopy(base.rendu, grille.pixel3, NULL, &grille.positionG))
                    goto Erreur;
                break;
        case CELLULE_Z6:
                if(0 != SDL_RenderCopy(base.rendu, grille.pixel6, NULL, &grille.positionG))
                    goto Erreur;
                break;
        case CELLULE_Z9:
                if(0 != SDL_RenderCopy(base.rendu, grille.pixel9, NULL, &grille.positionG))
                    goto Erreur;
                break;
    }
    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fc afficherBaseFenetre, affichage.c : %s\n", SDL_GetError());
    return -1;
}

int afficherBordsGrille(Rendu base)
{
    SDL_Rect pos = {0, 0, LARGEUR_GRILLE, 4};
    if(0 != SDL_RenderCopy(base.rendu, base.bordH, NULL, &pos))
        goto Erreur;
    pos.x = 0; pos.y = 4;
    pos.w = 4; pos.h = HAUTEUR_GRILLE;
    if(0 != SDL_RenderCopy(base.rendu, base.bordG, NULL, &pos))
        goto Erreur;

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fc afficherBords, affichage.c : %s\n", SDL_GetError());
    return -1;
}

int afficherTabBits(unsigned char **cells, DimTableaux dim, Cellules cellule, SDL_Renderer *renderer)
{

    for(unsigned int y = dim.debMargeH; y < dim.finMargeH; y++){
        for(unsigned int x = dim.debMargeL; x < dim.finMargeL; x++){
            if(cells[y][x] & 0x01){
                cellule.position.y = (y - dim.debMargeH) * cellule.type;
                cellule.position.x = (x - dim.debMargeL) * cellule.type;
                if(0 != SDL_RenderCopy(renderer, cellule.vivante, NULL, &cellule.position))
                    goto Erreur;
            }
        }
    }
    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fc afficherTabBits, affichage.c : %s\n", SDL_GetError());
    return -1;
}

/* Affiche le nombre de cellules vivantes. */
int afficherNombreCellVivantes(Iterations iter, const int nbVivantes, SDL_Renderer *renderer)
{
    iter.dst.y = CADREV_Y + 10;
    /* Met le nombre dans une chaîne de caractère. */
    sprintf(iter.car, "%d", nbVivantes);

    int i = 0;

    do{ /* Positionne src.x sur le bon chiffre puis l'affiche. */
        iter.src.x = (iter.car[i] - '0') * iter.src.w;
        i++;
        if(0 != SDL_RenderCopy(renderer, iter.chiffres, &iter.src, &iter.dst))
            goto Erreur;

        /* Décale dst d'un chiffre */
        iter.dst.x += iter.src.w;
    }while (iter.car[i] != '\0');

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fc afficherNombreIteration, affichage.c : %s\n", SDL_GetError());
    return -1;
}

/* Affiche le nombre des générations. */
int afficherNombreIteration(Iterations iter, SDL_Renderer *renderer)
{
    /* Met le nombre dans une chaîne de caractère. */
    sprintf(iter.car, "%d", iter.compteur);

    int i = 0;

    /* iter->dst.x -= strlen(iter->car) * (iter->src.w/2);  Centré */

    do{ /* Positionne src.x sur le bon chiffre puis l'affiche. */
        iter.src.x = (iter.car[i] - '0') * iter.src.w;
        i++;
        if(0 != SDL_RenderCopy(renderer, iter.chiffres, &iter.src, &iter.dst))
            goto Erreur;

        /* Décale dst d'un chiffre */
        iter.dst.x += iter.src.w;
    }while (iter.car[i] != '\0');

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fc afficherNombreIteration, affichage.c : %s\n", SDL_GetError());
    return -1;
}

int afficherBoutonRadioSelect(Boutons bouton, SDL_Renderer *renderer)
{
    if(0 != SDL_RenderCopy(renderer, bouton.radioV, NULL, &bouton.positionB[1]))
        goto Erreur;

    if(0 != SDL_RenderCopy(renderer, bouton.radioZ, NULL, &bouton.positionB[2]))
        goto Erreur;
    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fc afficherboutonRadioSelect, affichage.c : %s\n", SDL_GetError());
    return -1;
}

int afficherBarre(SDL_Texture *bar, SDL_Rect pos, SDL_Renderer *renderer)
{
    if(0 != SDL_RenderCopy(renderer, bar, NULL, &pos))
        goto Erreur;
    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fc afficherBarre, affichage.c : %s\n", SDL_GetError());
    return -1;
}

int misAJourAffichage(Programme jeu, SDL_Texture *tex, SDL_Rect pos)
{
    afficherBaseFenetre(jeu.base, jeu.grille, jeu.cellule.type);
    afficherNombreIteration(jeu.iter, jeu.base.rendu);
    afficherNombreCellVivantes(jeu.iter, jeu.cellule.nbVivantes, jeu.base.rendu);
    afficherBoutonRadioSelect(jeu.bouton, jeu.base.rendu);
    afficherBordsGrille(jeu.base);
    afficherBarre(tex, pos, jeu.base.rendu);
    return 0;
}
