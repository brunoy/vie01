#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include "const.h"
#include "struc.h"
#include "evenement.h"
#include "generation.h"
#include "action.h"
#include "fichiers_menu.h"
#include "fichiers_parse.h"
#include "fichiers_sauver.h"
#include "textures.h"
#include "affichage.h"
#include "demarrer.h"

static SDL_Texture *barreTexte;

void lancerLeJeu(Programme jeu, DimTableaux dim, Tableaux tab)
{
    /* ttf */
    TTF_Font *font = NULL;
    font = TTF_OpenFont(CHEMIN_FONT, TAILLE_LETTRES);
    if(font == NULL){
        printf("Défaut ouverture font 'TTF_Font', fichier -> demarrer.c");
        return;
    }

    /* Texture aide */
    SDL_Texture *aide = NULL;

    /* Barre texte */
    char texte[300] = "/patterns/";
    SDL_Rect barre = {70, HAUTEUR_GRILLE + 6, 0, 0};
    barreTexte = creerTexNomFichier(jeu.base.rendu, &barre, texte, font);

    /* Booléens */
    char quitter = 0, memo = 0, generer = 0, etat_aide = 0, etat_menu = 0, etat_svg = 0;

    /* Variables */
    char svg_survol = 0, etat_survol = 0, rafraichir = BASE;
    int x, y, retour = 0, tempsPrecedent = 0, tempsActuel = 0, tempo = 50;

    SDL_Event event;

    while (!quitter)
    {
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
            case SDL_QUIT:
                quitter = 1;
                break;
            /* ---------   Clavier   ----------------------------------------------- */
            case SDL_KEYDOWN:
                /* 'Q' --> Quitter. */
                if(event.key.keysym.sym == SDLK_q && event.key.keysym.mod & KMOD_SHIFT){
                    if(!etat_svg)
                        quitter = 1;
                }
                /* Controle du menu des patterns. */
                else if(etat_menu){

                    if(event.key.keysym.sym == SDLK_RETURN || event.key.keysym.sym == SDLK_KP_ENTER)
                    {
                        etat_menu = 0;
                        strcpy(texte, jeu.menu->cheminFichier[jeu.menu->cases]);

                        boutonEffacer(tab.map, dim, &etat_aide, &generer,
                                      &jeu.iter.compteur, &jeu.cellule.nbVivantes);

                        choixFonctionParse(jeu.menu->cheminFichier[jeu.menu->cases], tab.map,
                                           texte, dim, &jeu.cellule.nbVivantes);

                        if(barreTexte != NULL) SDL_DestroyTexture(barreTexte);
                        barreTexte = creerTexNomFichier(jeu.base.rendu, &barre, texte, font);
                    }
                    else if(event.key.keysym.sym == SDLK_UP){
                        fleche_up(&jeu.menu->survolM, &jeu.menu->deb, &jeu.menu->fin,
                                  jeu.menu->menuH);
                    }
                    else if(event.key.keysym.sym == SDLK_DOWN){
                        fleche_down(&jeu.menu->survolM, jeu.menu->fondM.h, jeu.menu->nbFichiers,
                                    &jeu.menu->deb, &jeu.menu->fin, jeu.menu->menuH);
                    }
                    else if(event.key.keysym.sym == SDLK_PAGEUP){
                        pageUp(jeu.menu->nbFichiers, &jeu.menu->deb, &jeu.menu->fin);
                    }
                    else if(event.key.keysym.sym == SDLK_PAGEDOWN){
                        pageDw(jeu.menu->nbFichiers, &jeu.menu->deb, &jeu.menu->fin);
                    }
                }
                /* Backspace zone de saisie 'sauvegarder'. */
                else if(event.key.keysym.sym == SDLK_BACKSPACE && etat_svg){
                    supprimerCaractere(jeu.svg.zoneText);
                    if(strlen(jeu.svg.zoneMess) > 1)
                            initMessage(jeu.svg.zoneMess);
                }
                /* Controle du jeu. */
                else if(event.key.keysym.sym == SDLK_SPACE && !etat_aide){
                        if(generer)
                            generer = 0;
                        else if(!boutonLancer(tab.map, dim, &memo))
                            generer = 1;
                        rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_SEMICOLON ){
                    boutonStopOuUn(tab.map, dim, &jeu.cellule, &jeu.iter.compteur, &memo, jeu.base.rendu);
                    rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_EXCLAIM && !etat_aide && !generer && !etat_svg){

                    if(barreTexte != NULL) SDL_DestroyTexture(barreTexte);
                    barreTexte = creerTexNomFichier(jeu.base.rendu, &barre, "/patterns/", font);

                    boutonAleatoire(tab.map, dim, &jeu.iter.compteur, &jeu.cellule.nbVivantes);
                    rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_PLUS || event.key.keysym.sym == SDLK_KP_PLUS){
                    if(vitesseClavier(&jeu.bouton, &jeu.iter.delai, &tempo, 1)){
                        rafraichir = BASE;
                    }
                }
                else if(event.key.keysym.sym == SDLK_MINUS || event.key.keysym.sym == SDLK_KP_MINUS){
                    if(vitesseClavier(&jeu.bouton, &jeu.iter.delai, &tempo, -1))
                        rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_LESS && event.key.keysym.mod & KMOD_LSHIFT){
                    if(zoomClavier(&jeu.bouton, &jeu.cellule.type, 1)){
                        majDimension(&dim, &jeu.cellule);
                        rafraichir = BASE;
                    }
                }
                else if(event.key.keysym.sym == SDLK_LESS){
                    if(zoomClavier(&jeu.bouton, &jeu.cellule.type, -1)){
                        majDimension(&dim, &jeu.cellule);
                        rafraichir = BASE;
                    }
                }
                else if(event.key.keysym.sym == SDLK_DELETE){
                    boutonEffacer(tab.map, dim, &etat_aide, &generer,
                                  &jeu.iter.compteur, &jeu.cellule.nbVivantes);
                    rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_UP){
                    if(deplacerCarte(&dim, 0, -1) && !generer)
                        rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_DOWN){
                    if(deplacerCarte(&dim, 0, 1) && !generer)
                        rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_RIGHT){
                    if(deplacerCarte(&dim, 1, 0) && !generer)
                        rafraichir = BASE;
                }
                else if(event.key.keysym.sym == SDLK_LEFT){
                    if(deplacerCarte(&dim, -1, 0) && !generer)
                        rafraichir = BASE;
                }
                break;
            /* --------------------   Molette   --------------------*/
            case SDL_MOUSEWHEEL:
                if(etat_menu)
                    molette(event.wheel.y, jeu.menu->nbFichiers, &jeu.menu->deb, &jeu.menu->fin);
                break;

            case SDL_MOUSEBUTTONDOWN:
            /* ----------  Appui bouton gauche souris.  ----------- */
                if(event.button.button == SDL_BUTTON_LEFT)
                {
                    if((retour = clicSurBouton(&jeu.bouton.positionB[0],
                                      event.button.x, event.button.y)))
                    {
                        if(etat_svg == 1){
                            rafraichir = BASE;
                        }
                        else if(retour != AIDE){
                            rafraichir = CLIC;
                        }
                        else if(etat_menu){
                            rafraichir = SURVOL;
                        }
                        else if(etat_aide){
                            if(AIDE != retour && EFFACER != retour && QUITTER != retour)
                                rafraichir = SURVOL;
                        }
                        else if(generer){
                            if(STOPPER != retour && QUITTER != retour)
                                rafraichir = SURVOL;
                        }
                    }
                }
                break;
            case SDL_MOUSEBUTTONUP:
            /* ----------  Bouton gauche souris relaché  ---------- */
                if(event.button.button == SDL_BUTTON_LEFT)
                {
                    x = event.button.x;
                    y = event.button.y;

                    /* Controle du menu. */
                    if(etat_menu){
                        if(clicMenu(jeu.menu, x, y, &etat_menu)){

                            strcpy(texte, jeu.menu->cheminFichier[jeu.menu->cases]);

                            boutonEffacer(tab.map, dim, &etat_aide, &generer,
                                          &jeu.iter.compteur, &jeu.cellule.nbVivantes);

                            choixFonctionParse(jeu.menu->cheminFichier[jeu.menu->cases],
                                               tab.map, texte, dim, &jeu.cellule.nbVivantes);

                            if(barreTexte != NULL) SDL_DestroyTexture(barreTexte);
                            barreTexte = creerTexNomFichier(jeu.base.rendu, &barre, texte, font);
                        }
                    }
                    /* Controle de la sauvegarde du pattern. */
                    else if(etat_svg){
                        if((retour = clicSaisie(x, y, &svg_survol))){
                            if(retour == 1){
                                etat_svg = 0;
                                initMessage(jeu.svg.zoneMess);
                                initChaine(jeu.svg.zoneText);
                            }
                            else{
                                if(!boutonSauvegarder(dim, jeu.menu, tab.map, jeu.svg.pattHome,
                                                jeu.svg.zoneText, jeu.svg.zoneMess)){
                                    etat_svg = 0;
                                }
                            }
                        }
                    }
                    /* Controle remplissage grille. */
                    else if(clicSurGrille(jeu.cellule.type, dim.debMargeH, dim.debMargeL, &x, &y)){
                        if(!etat_aide){
                            if(!cell_state(tab.map, x, y)){
                                activeCellule(tab.map, dim, x, y, &jeu.cellule.nbVivantes);
                            }
                            else{
                                desactiveCellule(tab.map, dim, x, y, &jeu.cellule.nbVivantes);
                            }
                            if(!memo) memo = 1;
                            rafraichir = BASE;
                        }
                    }
                    /* Controle des boutons. */
                    else if((retour = clicSurBouton(&jeu.bouton.positionB[0], x, y)))
                    {
                        if(retour == AIDE)
                            rafraichir = BASE;
                        else
                            rafraichir = SURVOL;

                        if(AFFICHER == retour && !etat_aide && !etat_svg && jeu.menu->nbFichiers){
                            boutonAfficher(&generer, &etat_menu, &etat_survol);
                        }
                        else if(SAUVER == retour){
                            if(!generer && !etat_menu && !etat_aide && !etat_svg)
                                etat_svg = 1;
                        }
                        else if(ALEATOIRE == retour && !etat_aide && !generer && !etat_svg){
                            if(barreTexte != NULL){
                                SDL_DestroyTexture(barreTexte);
                            }
                            barreTexte = creerTexNomFichier(jeu.base.rendu, &barre, "/patterns/", font);
                            boutonAleatoire(tab.map, dim, &jeu.iter.compteur, &jeu.cellule.nbVivantes);
                        }
                        else if(DERNIERJOUE == retour && !etat_aide && !generer){
                            if(barreTexte != NULL){
                                SDL_DestroyTexture(barreTexte);
                            }
                            barreTexte = creerTexNomFichier(jeu.base.rendu, &barre, "/patterns/", font);
                            boutonDernierJeu(tab.map, dim, &jeu.iter.compteur, &jeu.cellule.nbVivantes);
                        }
                        else if(STOPPER == retour){
                            if(generer)
                                generer = 0;
                            else
                                boutonStopOuUn(tab.map, dim, &jeu.cellule, &jeu.iter.compteur,
                                               &memo, jeu.base.rendu);
                        }
                        else if(LANCER == retour && !etat_aide && !generer){
                            if(!boutonLancer(tab.map, dim, &memo))
                                generer = 1;
                        }
                        else if(EFFACER == retour){
                            boutonEffacer(tab.map, dim, &etat_aide, &generer,
                                          &jeu.iter.compteur, &jeu.cellule.nbVivantes);

                            if(barreTexte != NULL) SDL_DestroyTexture(barreTexte);
                            barreTexte = creerTexNomFichier(jeu.base.rendu, &barre, "/patterns/", font);
                        }
                        else if(QUITTER == retour){
                            if(generer) generer = 0;
                            quitter = 1;
                        }
                        else if(AIDE == retour){
                            boutonAide(&aide, jeu.base.rendu, &etat_aide);
                        }
                    }
                    /* Controle des boutons radio vitesse. */
                    else if((retour = clicBoutonRadioVitesse(x, y))){
                        if(boutonRadioVitesse(&jeu.bouton, &jeu.iter.delai, &tempo,  --retour)){
                            rafraichir = BASE;
                        }
                    }
                    /* Controle des boutons radio zoom. */
                    else if((retour = clicBoutonRadioZoom(x, y))){
                        if(boutonRadioZoom(&jeu.bouton, &jeu.cellule.type, --retour)){
                            majDimension(&dim, &jeu.cellule);
                            rafraichir = BASE;
                        }
                    }
                }
                break;

            case SDL_MOUSEMOTION:
            /* Leger changement de la couleur au survol de la souris ...  */
                /* ... des boutons boite de sauvegarde. */
                if(etat_svg){
                    survolSaisie(event.motion.x, event.motion.y, &svg_survol);
                }
                /* ... du menu des patterns. */
                else if(etat_menu){
                    survolMenu(jeu.menu, &etat_survol, event.motion.x, event.motion.y);
                }
                /* Si bouton gauche souris appuyé, rempli la grille. */
                else if(event.motion.state & SDL_BUTTON_LMASK && !etat_aide){
                    if((event.motion.x < LARGEUR_GRILLE && event.motion.y < HAUTEUR_GRILLE)){
                        x = (event.motion.x/jeu.cellule.type) + dim.debMargeL;
                        y = (event.motion.y/jeu.cellule.type) + dim.debMargeH;
                        if(!(tab.map[y][x] & 0x01)){
                            activeCellule(tab.map, dim, x, y, &jeu.cellule.nbVivantes);
                        }
                        rafraichir = BASE;
                    }
                }
                /* Si bouton droit souris appuyé et carte zoomée, déplace cette dernière. */
                else if(event.motion.state & SDL_BUTTON_RMASK){
                    if(deplacerCarte(&dim, event.motion.xrel, event.motion.yrel) && !generer)
                        rafraichir = BASE;
                }
                /* ... des boutons de la fenêtre. */
                else if((retour = survolBouton(&jeu.bouton.positionB[0], &etat_survol, event.motion.x, event.motion.y)))
                {
                    rafraichir = retour;
                }
                break;

            case SDL_TEXTINPUT:
            /* Récupère la saisie clavier pour la boite sauvegarder. */
                if(etat_svg){
                    if(!verifMaxChaine(jeu.svg.zoneText, jeu.svg.zoneMess)){
                        ajoutCaractere(jeu.svg.zoneText, event.text.text);
                        if(strlen(jeu.svg.zoneMess) > 1)
                            initMessage(jeu.svg.zoneMess);
                    }
                }
                break;
            case SDL_WINDOWEVENT:
                /* Si fenêtre restaurée, focus et rafraichissement */
                if(event.window.event == SDL_WINDOWEVENT_MINIMIZED)
                {
                    SDL_RaiseWindow(jeu.sdl.fenetre);
                    rafraichir = BASE;
                }
                break;
            }
        }


        if(generer){
            tempsActuel=SDL_GetTicks();
            if (tempsActuel-tempsPrecedent > jeu.iter.delai){
                misAJourAffichage(jeu, barreTexte, barre);
                jeu.iter.compteur++;
                genererNextCellules(tab.map, dim, &jeu.cellule, jeu.base.rendu);

                /* Pour le rendu des boutons */
                if(etat_survol)
                    rafraichir = etat_survol;
                else if(etat_aide)
                    rafraichir = BASE;
                else
                    SDL_RenderPresent(jeu.base.rendu);

                tempsPrecedent=tempsActuel;
            }
        }

        if(rafraichir){

            misAJourAffichage(jeu, barreTexte, barre);
            afficherTabBits(tab.map, dim, jeu.cellule, jeu.base.rendu);

            if(rafraichir == SURVOL)
                SDL_RenderCopy(jeu.base.rendu, jeu.bouton.survol, NULL, &jeu.bouton.positionB[0]);
            else if(rafraichir == QUIT)
                SDL_RenderCopy(jeu.base.rendu, jeu.bouton.quitte, NULL, &jeu.bouton.positionB[0]);
            else if(rafraichir == CLIC)
                SDL_RenderCopy(jeu.base.rendu, jeu.bouton.clique, NULL, &jeu.bouton.positionB[0]);

            if(etat_menu){
                enteteMenu(jeu.menu,  jeu.base.rendu, font);
                pageMenu(jeu.menu, jeu.base.rendu, font);
                choixMenu(jeu.menu, jeu.base.rendu, font);
            }
            else if(etat_aide){
                SDL_RenderCopy(jeu.base.rendu, aide, NULL, &jeu.grille.positionG);
                afficherBordsGrille(jeu.base);
            }
            else if(etat_svg){
                boiteDeSaisie(jeu.base.rendu, svg_survol);
                texteBoiteSaisie(jeu.base.rendu, font, &jeu.svg);
            }

            SDL_RenderPresent(jeu.base.rendu);
            if(!etat_aide && !etat_menu && !etat_svg)
                rafraichir = NON;
        }
        SDL_Delay(tempo);
    }
    /* Quitte. */
    if(aide != NULL) SDL_DestroyTexture(aide);
    if(barreTexte != NULL) SDL_DestroyTexture(barreTexte);
    free(jeu.svg.pattHome);
    TTF_CloseFont(font);
}
