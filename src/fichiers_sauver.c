#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <locale.h>
#include <libintl.h>
#include <sys/stat.h>
#ifdef _WIN32
    #include <fcntl.h>
#endif

#include "const.h"
#include "struc.h"
#include "textures.h"
#include "memoire.h"
#include "fichiers_menu.h"
#include "fichiers_recherche.h"
#include "fichiers_sauver.h"

int boiteDeSaisie(SDL_Renderer *renderer, char svg_survol)
{
    /* Contour. */
    SDL_Rect boite = {BOITE_X - 3, BOITE_Y - 3, BOITE_LARG + 6, BOITE_HAUT + 6};
    if(0 != SDL_SetRenderDrawColor(renderer, 128, 139, 150, 255)) goto Erreur;
    if(0 != SDL_RenderFillRect(renderer, &boite)) goto Erreur;

    /* Boite. */
    //SDL_Rect boite = {BOITE_X, BOITE_Y, BOITE_LARG, BOITE_HAUT};
    boite.x = BOITE_X; boite.y = BOITE_Y; boite.w = BOITE_LARG; boite.h = BOITE_HAUT;
    if(0 != SDL_SetRenderDrawColor(renderer, 214, 219, 223, 255)) goto Erreur;
    if(0 != SDL_RenderFillRect(renderer, &boite)) goto Erreur;

    /* Zone de saisie. */
    if(0 != SDL_SetRenderDrawColor(renderer, 250, 250, 250, 255)) goto Erreur;
    boite.x = ZONE_X;
    boite.y = ZONE_Y;
    boite.w = ZONE_LARG;
    boite.h = ZONE_HAUT;
    if(0 != SDL_RenderFillRect(renderer, &boite)) goto Erreur;

    /* Bouton annuler. */
    if(0 != boutonAnnuler(renderer, svg_survol)) goto Erreur;

    /* Bouton valider. */
    if(0 != boutonValider(renderer, svg_survol)) goto Erreur;

    return 0;

Erreur:
    fprintf(stderr, "Erreur SDL, Fonction boite, fichier fichiers_sauver.c : %s\n", SDL_GetError());
    return -1;
}

int boutonAnnuler(SDL_Renderer *renderer, char svg_survol)
{
    if(svg_survol == ANNULER){
        if(0 != SDL_SetRenderDrawColor(renderer, 210,77, 87, 255)) goto Erreur;
    }
    else{
        if(0 != SDL_SetRenderDrawColor(renderer, 128,139, 150, 255)) goto Erreur;
    }

    /* Bouton annuler. */
    SDL_Rect boite = {BOITE_X + BOITE_BT_BORD, BOITE_BT_Y, LARG_BOUTON, HAUT_BOUTON};
    if(0 != SDL_RenderFillRect(renderer, &boite)) goto Erreur;

    /* Icone. */
    int x = BOITE_X + BOITE_BT_BORD+12, y = BOITE_BT_Y+2;
    if(0 != SDL_SetRenderDrawColor(renderer, 250,250, 250, 255)) goto Erreur;

    for(int i = 0; i < HAUT_BOUTON-4; i++){
        SDL_RenderDrawPoint(renderer, x+10-(i*2), y);
        SDL_RenderDrawPoint(renderer, x+11-(i*2), y);
        SDL_RenderDrawPoint(renderer, x+1, y);
        SDL_RenderDrawPoint(renderer, x++, y++);
    }
    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, Fonction survolBoutonSaisie, fichier fichiers_sauver.c : %s\n", SDL_GetError());
    return -1;
}

int boutonValider(SDL_Renderer *renderer, char svg_survol)
{
    if(svg_survol == VALIDER){
        if(0 != SDL_SetRenderDrawColor(renderer, 69, 179, 157, 255)) goto Erreur;
    }
    else{
        if(0 != SDL_SetRenderDrawColor(renderer, 128, 139, 150, 255)) goto Erreur;
    }

    /* Bouton valider. */
    SDL_Rect boite = {(BOITE_LARG + BOITE_X) - LARG_BOUTON - BOITE_BT_BORD, BOITE_BT_Y, LARG_BOUTON, HAUT_BOUTON};
    if(0 != SDL_RenderFillRect(renderer, &boite)) goto Erreur;

    /* Icone. */
    if(0 != SDL_SetRenderDrawColor(renderer, 250,250, 250, 255)) goto Erreur;
    int x = (BOITE_LARG + BOITE_X) - LARG_BOUTON - BOITE_BT_BORD + 6;
    int y = BOITE_BT_Y + (HAUT_BOUTON/2);

    for(int i = 0; i < HAUT_BOUTON/2 -2; i++){
        SDL_RenderDrawPoint(renderer, x+1, y);
        SDL_RenderDrawPoint(renderer, x++, y++);
    }
    for(int i = 0; i < HAUT_BOUTON-6; i++){
        SDL_RenderDrawPoint(renderer, x+1, y);
        SDL_RenderDrawPoint(renderer, x+=2, y--);
    }
    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, Fonction survolBoutonSaisie, fichier fichiers_sauver.c : %s\n", SDL_GetError());
    return -1;
}

/* Déplacement du texte arrivé en bout de zone de saisie. */
void deplacementText(SDL_Rect *srcT, SDL_Rect *dstT)
{
    if(dstT->w < ZONE_LARG){
        srcT->w = dstT->w;
        srcT->x = 0;
        srcT->h = dstT->h;
    }
    else{
        srcT->x = dstT->w - ZONE_LARG;
        srcT->w = dstT->w = ZONE_LARG;
    }
}

/* Ajoute un caractère à la chaine. */
void ajoutCaractere(char *chaine, const char *car)
{
    chaine[strlen(chaine) - 1] = '\0';
    strcat(chaine, car);
    strcat(chaine, "|");
}

void supprimerCaractere(char *chaine)
{
    /* Removing multi-byte characters from the UTF-8 string. */
    int longueur = strlen(chaine) - 1;
    while (longueur && chaine[longueur - 1] < -64) {
        longueur--;
    }
    if (longueur) {
        longueur--;
    }

    chaine[longueur] = '|';
    chaine[longueur+1] = 0;
}

/* Réinitialise la chaine. */
void initChaine(char *chaine)
{
    chaine[0] = '|';
    chaine[1] = '\0';
}

void initMessage(char *message)
{
    message[0] = ' ';
    message[1] = '\0';
}

/* Vérifie que le nombre de caractères entrent dans la chaine et cas échéant affiche un message.*/
int verifMaxChaine(const char *chaine, char *message)
{
    if(strlen(chaine) > 95){
        strcpy(message, _("Maxi caractères atteint !"));
        return 1;
    }
    return 0;
}

/* Afficher texte boite de sauvegarde. */
int texteBoiteSaisie(SDL_Renderer *renderer, TTF_Font *font, Sauvegarder *svg)
{
    SDL_Color cl = {33, 47, 61, 255};

    /* Affiche le texte 'Entrez un nom'.*/
    ecrireTexte(ZONE_X, BOITE_Y + HAUT_BOUTON/4, renderer, cl, font, _("Entrez un nom"));

    /* Affiche un message d' erreur. */
    ecrireTexte(ZONE_X, ZONE_Y + 25, renderer, cl, font, svg->zoneMess);

    /* Affiche les caractères saisis . */
    SDL_Texture *texte = NULL;
    texte = text(renderer, cl, font, &svg->dstT, svg->zoneText);
    if(texte == NULL) goto Erreur;

    deplacementText(&svg->srcT, &svg->dstT);

    if(0 != SDL_RenderCopy(renderer, texte, &svg->srcT, &svg->dstT)) goto Erreur;

    SDL_DestroyTexture(texte);
    texte = NULL;

    return 0;
Erreur:
    if(NULL != texte)
        SDL_DestroyTexture(texte);
    fprintf(stderr, "Erreur SDL, fn texteBoiteSaisie, fichiers_sauver.c : %s\n", SDL_GetError());
    return -1;
}

char dir(const char *name)
{
    struct stat stbuf;
    if (stat(name, &stbuf) == -1) {
        return -1;
    }
    return ((stbuf.st_mode & S_IFMT) == S_IFDIR);
}

/* Concatenation chemin, nom et extension. */
char *concat(const char *nom, const char *monRep)
{
    const char *monPatt = DOSPERSO;
    const char *ext = ".rle";
    char *nomChemin = NULL;

    nomChemin = malloc(strlen(monRep) + strlen(monPatt) + strlen(nom) + strlen(ext) + 1 * sizeof(char) );
    if(nomChemin == NULL) { fprintf(stderr, "Erreur memoire\n"); return NULL; }

    strcpy(nomChemin, monRep);
    strcat(nomChemin, monPatt);
    strcat(nomChemin, nom);
    strcat(nomChemin, ext);

    return nomChemin;
}

int verifNomPresent(char **cheminFichier, const char *monPattern, const int nbFichiers)
{
    for(int i = 0; i < nbFichiers; i++){
        if(!strcmp(cheminFichier[i], monPattern)){
            return 1;
        }
    }
    return 0;
}

int ecrireFichieRLE(DimTableaux dim, unsigned char **map, const char *nom, const char *monPattern)
{
    int x1 = dim.largeur, x2 = 0, y1 = dim.hauteur, y2 = 0;
    celluleDebutFin(map, dim, &x1, &y1, &x2, &y2);

    x2 += 1; /* Ex : x2 = 2 (0 à 2) --> +1 3 valeurs. */
    y2 += 1;

    if(monPattern == NULL){
        perror(monPattern);
        return -1;
    }

    FILE* fichier = NULL;
    fichier = fopen(monPattern, "w+");

    if(NULL == fichier){
        printf("Erreur ouverture fichier\n");
        return -1;
    }


    fprintf(fichier, "#N %s\n", nom);
    fprintf(fichier, "#O prog vie01\n");
    fprintf(fichier, "x = %d, y = %d, rule = b3/s23\n", x2-x1, y2-y1);

    char *motif = NULL, buffer[16]= {0};

    /* Largeur max conseillé au format rle (70), + le \0 de fin. */
    motif = malloc(LARG_MAX_RLE + 1 * sizeof(char));

    if(motif == NULL){
        perror(motif);
        return -1;
    }
    *motif = 0;

    int nombre_bytes = 0;

    int x, y, o = 0, b = 0;

    for(y = y1; y < y2; y++){
        for(x = x1; x < x2; x++){
            if(map[y][x] & 0x01){
                o += 1;
                if(!celluleSuivante(map, x, y, x1, x2, y2) || x == x2 -1){
                    if(o > 1)
                        nombre_bytes += sprintf(buffer, "%do", o);
                    else
                        nombre_bytes += sprintf(buffer, "o");
                    o = 0;
                }
            }
            else{
                b += 1;
                if(celluleSuivante(map, x, y, x1, x2, y2) > 0 || x == x2 -1){
                    if(b > 1)
                        nombre_bytes += sprintf(buffer, "%db", b);
                    else
                        nombre_bytes += sprintf(buffer, "b");
                    b = 0;
                }
            }

            if(*buffer){
                if(nombre_bytes >= LARG_MAX_RLE){
                    fprintf(fichier, "%s\n", motif);
                    *motif = 0;
                    nombre_bytes = strlen(buffer);
                }
                strcat(motif, buffer);
                *buffer = 0;
            }
        }
        if((y == y2 - 1) || (x == x2 - 1))
            strcat(motif, "!");
        else{
            nombre_bytes ++;
            strcat(motif, "$");
        }
    }
    if(*motif)
        fprintf(fichier, "%s", motif);


    fclose(fichier);
    free(motif);
    return 0;
}

int celluleSuivante(unsigned char **map, int x, int y, int x1, int x2, int y2)
{
    if(x < x2 - 1){
        return map[y][x+1] & 0x01;
    }
    if(y < y2 - 1){
        return map[y+1][x1] & 0x01;
    }
    return -1; /* Fin de parcours map */
}

void celluleDebutFin(unsigned char **map, DimTableaux dim, int *x1, int *y1, int *x2, int *y2)
{
    for(int y = 0; y < (int)dim.hauteur; y++){
        for(int x = 0; x < (int)dim.largeur; x++){
            if(map[y][x] & 0x01){
                if(*x1 > x) *x1 = x;
                if(*y1 > y) *y1 = y;
                if(*x2 < x) *x2 = x;
                if(*y2 < y) *y2 = y;
            }
        }
    }
}

int ajoutPatternBase(MenuDyn *menu, char *pattern)
{
    if(menu->nbFichiers < MAX_FICHIERS)
        menu->nbFichiers += 1;
    else{
        printf("Limite nombre maxi patterns atteint");
        return -1;
    }

    /* Realloue */
    if(reallocMem(&menu->cheminFichier, menu->nbFichiers)) return -1;

    menu->cheminFichier[menu->nbFichiers-1] = NULL;
    menu->cheminFichier[menu->nbFichiers-1] = strdup(pattern);

    if(NULL == menu->cheminFichier[menu->nbFichiers-1]){
        printf("Erreur strdup realloc");
        return -1;
    }

    /* Tri. */
    qsort (menu->cheminFichier, menu->nbFichiers, sizeof *menu->cheminFichier, compare);

     /* Redétermine hauteur et largeur du menu */
    tailleCaseMenu(menu->cheminFichier, &menu->menuW, &menu->menuH, menu->nbFichiers);

    int nombreCaseMenu = (menu->nbFichiers > ELEMENTS_MAX) ? ELEMENTS_MAX : menu->nbFichiers;

    /* Valeur parcours tableau */
    menu->deb = 0;
    menu->fin = nombreCaseMenu;
    menu->cases = 0;

    /* Font menu */
    menu->fondM.x = POSX_MENU(menu->menuW) - MARGE;
    menu->fondM.y = POSY_MENU;
    menu->fondM.w = menu->menuW;
    menu->fondM.h = menu->menuH * nombreCaseMenu;
    /* Taille et position menu */
    menu->posM.x = POSX_MENU(menu->menuW);
    menu->posM.y = POSY_MENU;
    menu->posM.w = menu->menuW;
    menu->posM.h = menu->menuH;
    /* Menu survol */
    menu->survolM.x = POSX_MENU(menu->menuW) - MARGE;
    menu->survolM.y = POSY_MENU;
    menu->survolM.w = menu->menuW;
    menu->survolM.h = menu->menuH;

    return 0;
}
