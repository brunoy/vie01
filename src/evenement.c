#include <SDL2/SDL.h>
#include "const.h"
#include "struc.h" // pour enum boutons
#include "evenement.h"


/* -------------------------------------------------------------------------- *
 *                             Gère la grille                                 *
 * -------------------------------------------------------------------------- */

int clicSurGrille(int zoom, int margeH, int margeL, int *x, int *y)
{
    if (*x > LARGEUR_GRILLE - 1)
        return 0;
    if (*y > HAUTEUR_GRILLE - 1)
        return 0;

    *x = (*x / zoom) + margeL;
    *y = (*y / zoom) + margeH;

    return 1;
}

/* ---------------------------------------------------------------------------------- *
 *                       Gère les boutons du tableau de bord                          *
 * ---------------------------------------------------------------------------------- */

int survolBouton(SDL_Rect *position, char *etatSurvol, int x, int y)
{
    if(x > BOUTON_GX && x < BOUTON_GX + LARG_BOUTON)
        position->x = BOUTON_GX;
    else if(x > BOUTON_DX && x < BOUTON_DX + LARG_BOUTON)
        position->x = BOUTON_DX;
    else
    {
        if(*etatSurvol) /* Évitera d'effectuer un rendercopy à chaque mouvement de souris */
            {
                *etatSurvol = 0;
                return BASE; /* Bouton normal */
            }
        return 0; /* Rien à faire */
    }

    if(quelBouton(position, y))
    {
        if(*etatSurvol == 0)
        {
            if(quelBouton(position, y) == QUITTER){
                *etatSurvol = 3;
                return QUIT; /* bouton quitter */
            }
            else{
                *etatSurvol = 2;
                return SURVOL; /* Bouton survol */
            }
        }
    }
    else
    {
        if(*etatSurvol)
        {
            *etatSurvol = 0;
            return BASE; /* bouton normal */
        }
    }
    return 0;
}

int clicSurBouton(SDL_Rect *position, int x, int y)
{
    if(x > BOUTON_GX && x < BOUTON_GX + LARG_BOUTON)
        position->x = BOUTON_GX;
    else if(x > BOUTON_DX && x < BOUTON_DX + LARG_BOUTON)
        position->x = BOUTON_DX;
    else if(x > BOUTON_AIDE_X && y > BOUTON_AIDE_Y)
        return AIDE;
    else
        return 0;
    return quelBouton(position, y);
}

int quelBouton(SDL_Rect *p, int y)
{
    if(y > CADRE2_Y + ESP_BOUTON_Y && y < CADRE2_Y + ESP_BOUTON_Y + HAUT_BOUTON){
        p->y = CADRE2_Y + ESP_BOUTON_Y;
        return (p->x == BOUTON_GX) ? AFFICHER : SAUVER;
    }
    if(y > CADRE3_Y + ESP_BOUTON_Y && y < CADRE3_Y + ESP_BOUTON_Y + HAUT_BOUTON){
        p->y = CADRE3_Y + ESP_BOUTON_Y;
        return (p->x == BOUTON_GX) ? ALEATOIRE : DERNIERJOUE;
    }
    if(y > CADRE6_Y + ESP_BOUTON_Y && y < CADRE6_Y + ESP_BOUTON_Y + HAUT_BOUTON){
        p->y = CADRE6_Y + ESP_BOUTON_Y;
        return (p->x == BOUTON_GX) ? STOPPER : LANCER;
    }
    if(y > CADRE7_Y + ESP_BOUTON_Y && y < CADRE7_Y + ESP_BOUTON_Y + HAUT_BOUTON){
        p->y = CADRE7_Y + ESP_BOUTON_Y;
        return (p->x == BOUTON_GX) ? EFFACER : QUITTER;
    }
    return 0;
}

/* --------------------------------------------------------------------------- *
 *                            Gère les boutons radio                           *
 * --------------------------------------------------------------------------- */

int clicBoutonRadioZoom(int x, int y)
{
    if(y > CADRE4_Y + ESP_RADIO_Y && y < CADRE4_Y + ESP_RADIO_Y + BOUTON_RADIO)
    {
        int tmp = CADRE_X + ESP_RADIO_XA;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 1;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 2;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 3;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 4;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 5;
    }
    return 0;
}

int clicBoutonRadioVitesse(int x, int y)
{
    if(y > CADRE5_Y + ESP_RADIO_Y && y < CADRE5_Y + ESP_RADIO_Y + BOUTON_RADIO)
    {
        int tmp = CADRE_X + ESP_RADIO_XA;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 1;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 2;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 3;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 4;

        tmp += BOUTON_RADIO + ESP_RADIO_XB;
        if(x > tmp && x < tmp + BOUTON_RADIO)
            return 5;
    }
    return 0;
}

/* --------------------------------------------------------------------------- *
 *                Gère le menu des fichier.lif/.rle                            *
 * --------------------------------------------------------------------------- */

void survolMenu(MenuDyn *menu, char *etatSurvol, int x, int y)
{
    if(x < POSX_MENU(menu->menuW) || x > POSX_MENU(menu->menuW) + menu->menuW || y < POSY_MENU ||
                        y > POSY_MENU + menu->fondM.h){
        if(*etatSurvol){
            *etatSurvol = 0;
        }
        return;
    }

    for(int i = 0, j = POSY_MENU; i < menu->fondM.h/menu->menuH; i++){
        j += menu->menuH;
        if(y < j){
            if(*etatSurvol != i){
                *etatSurvol = i;
                menu->survolM.y = j - menu->menuH;
                break;
            }
            else
                return;
        }
    }
    return;
}

int fleche_up(SDL_Rect *survolM, int *deb, int *fin, int menuH)
{
    if(survolM->y == POSY_MENU){
        if(*deb > 0){
            *deb -= 1;
            *fin -= 1;
            return 1;
        }
        else
            return 0;
    }
    survolM->y -=  menuH;
    return 0;
}

int fleche_down(SDL_Rect *survolM, int mMax, int nFic, int *deb, int *fin, int menuH)
{
    if(survolM->y == (mMax-menuH) + POSY_MENU){
        if(*fin < nFic){
            *deb += 1;
            *fin += 1;
            return 1;
        }
        else
            return 0;
    }
    survolM->y += menuH;
    return 0;
}

void molette(int y, int nbFics, int *deb, int *fin)
{
    if(nbFics <=ELEMENTS_MAX)
        return;

    if(y > 0){ /* Page up */
        if(*deb > ELEMENTS_MAX){
            *deb -= ELEMENTS_MAX;
            *fin -= ELEMENTS_MAX;
        }
        else{
            *deb = 0;
            *fin = ELEMENTS_MAX;
        }
    }
    else if(y < 0){ /* Page down */
        if(nbFics == *fin)
            return;
        else if(nbFics - *fin > ELEMENTS_MAX){
            *deb += ELEMENTS_MAX;
            *fin += ELEMENTS_MAX;
        }
        else{
            *deb = nbFics - ELEMENTS_MAX;
            *fin = nbFics;
        }
    }
    return;
}

void pageUp(int nbFics, int *deb, int *fin)
{
    if(nbFics <=ELEMENTS_MAX)
        return;

    if(*deb > ELEMENTS_MAX){
        *deb -= ELEMENTS_MAX;
        *fin -= ELEMENTS_MAX;
    }
    else{
        *deb = 0;
        *fin = ELEMENTS_MAX;
    }
}

void pageDw(int nbFics, int *deb, int *fin)
{
    if(nbFics <=ELEMENTS_MAX)
        return;

    if(nbFics == *fin)
        return;
    else if(nbFics - *fin > ELEMENTS_MAX){
        *deb += ELEMENTS_MAX;
        *fin += ELEMENTS_MAX;
    }
    else{
        *deb = nbFics - ELEMENTS_MAX;
        *fin = nbFics;
    }
}

int clicMenu(MenuDyn *menu, int x, int y, char *etatMenu)
{
    int element = 0;

    if(x > POSX_MENU(menu->menuW) &&\
       x < POSX_MENU(menu->menuW) + menu->menuW &&\
       y > POSY_MENU && y < POSY_MENU + menu->fondM.h)
    { /* Si clic sur un élément du menu */
        ++element;
    }
    /* Si clic n'importe ou ailleurs, on quitte le menu */
    *etatMenu = 0;
    return element;
}

/* --------------------------------------------------------------------------- *
 *                Gère la boite de saisie de sauvegarde.                       *
 * --------------------------------------------------------------------------- */

int survolSaisie(int x, int y, char *svg_survol)
{
    if(y >= BOITE_BT_Y && y <= BOITE_BT_Y + HAUT_BOUTON){
        if(x >= BOITE_X + BOITE_BT_BORD && x <= BOITE_X + BOITE_BT_BORD + LARG_BOUTON){
            if(!*svg_survol) *svg_survol = ANNULER;
            return 0;
        }
        else if(x > (BOITE_X + BOITE_LARG) - (BOITE_BT_BORD + LARG_BOUTON) &&
                x < (BOITE_X + BOITE_LARG) - BOITE_BT_BORD){
            if(!*svg_survol) *svg_survol = VALIDER;
            return 0;
        }
    }
    if(*svg_survol) *svg_survol = 0;
    return 0;
}

int clicSaisie(int x, int y, char *svg_survol)
{
    if(y >= BOITE_BT_Y && y <= BOITE_BT_Y + HAUT_BOUTON){
        if(x >= BOITE_X + BOITE_BT_BORD && x <= BOITE_X + BOITE_BT_BORD + LARG_BOUTON){
            if(*svg_survol) *svg_survol = 0;
            return 1; /* Annuler. */
        }
        else if(x > (BOITE_X + BOITE_LARG) - (BOITE_BT_BORD + LARG_BOUTON) &&
                x < (BOITE_X + BOITE_LARG) - BOITE_BT_BORD){
            if(*svg_survol) *svg_survol = 0;
            return 2; /* Sauvegarder. */
        }
    }
    return 0;
}

/* --------------------------------------------------------------------------- *
 *                    Déplacement de la carte si zoomée.                       *
 * --------------------------------------------------------------------------- */

int deplacerCarte(DimTableaux *dim, int relX, int relY)
{
    relX *= -1;
    relY *= -1;
    int statut = 0;

    if((int)dim->debMargeL + relX >= 0 && dim->finMargeL + relX < LARGEUR_GRILLE){
        dim->debMargeL += relX;
        dim->finMargeL += relX;
        statut = 1;
    }
    if((int)dim->debMargeH + relY >= 0 && dim->finMargeH + relY < HAUTEUR_GRILLE){
        dim->debMargeH += relY;
        dim->finMargeH += relY;
        statut = 1;
    }
    return statut;
}
