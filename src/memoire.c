#include <stdlib.h>
#include <stdio.h>
#include "const.h"
#include "memoire.h"


/* Allocation mémoire tableau 2 dimensions. */
int allocMem(unsigned char ***ptr)
{
    if(*ptr != NULL) // pour éviter les fuites memoires
    {
        printf("*ptr non NULL\n");
        return -1;
    }

    unsigned char *tmp = NULL;
    unsigned int i;

    tmp = malloc(LARGEUR_GRILLE/CELLULE * HAUTEUR_GRILLE/CELLULE * sizeof(unsigned char));
    if(tmp == NULL)
    {
        printf("Erreur alloc memoire tmp");
        return -1;
    }

    (*ptr) = malloc(HAUTEUR_GRILLE/CELLULE * sizeof(unsigned char*));
    if((*ptr) == NULL)
    {
        printf("Erreur alloc memoire (*ptr)");
        return -1;
    }

    for(i = 0; i < HAUTEUR_GRILLE/CELLULE; i++)
    {
        (*ptr)[i] = &tmp[(i * sizeof(unsigned char)) * (LARGEUR_GRILLE/CELLULE)];
    }
    return 0;
}

/* Allocation mémoire tableau chaine 1ere dimension. */
int allocMemChaine(char ***ptr, const int taille)
{
    if(*ptr != NULL) // pour éviter les fuites memoires
    {
        printf("*ptr non NULL alloc chaine\n");
        return -1;
    }

    (*ptr) = malloc (taille * sizeof (*ptr));

    if((*ptr) == NULL)
    {
        printf("Erreur alloc memoire chaine (*ptr)");
        return -1;
    }

    return 0;
}

int reallocMem(char ***ptr, const int taille)
{
    char **tmp = NULL;
    tmp = realloc(*ptr, taille * sizeof (*tmp));

    if(tmp == NULL)
    {
        printf("Echec realloc memoire.");
        return -1;
    }

    *ptr = tmp;
    return 0;
}
