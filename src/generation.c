#include <SDL2/SDL.h>
//#include <SDL2/SDL_ttf.h>
#include <stdlib.h>
#include <stdio.h>
#include "const.h"
#include "struc.h"
#include "generation.h"

/*** Optimisation de l'algorithme du jeu de la vie basée sur le chapitre 17 du livre
 * Michael Abrash’s Graphics Programming Black Book, Special Edition
 * listing 17-5  ***/

/* initialise à zéro sauf le bit contenant le tableau dernier joué. */
void initTab(unsigned char **cells, DimTableaux dim)
{
    for(unsigned int i= 0; i < dim.hauteur; i++)
    {
        for(unsigned int j = 0; j < dim.largeur; j++)
        {
            cells[i][j] &= ~0xBF;
        }
    }
}

/* mémorise */
void copieMemo(unsigned char **cells, DimTableaux dim)
{
    for (unsigned int y = 0; y < dim.hauteur; y++) {
        for (unsigned int x = 0; x < dim.largeur; x++) {
            if(cells[y][x] & 0x01){
                cells[y][x] |= 0x40;
            }
            else
                cells[y][x] &= ~0x40;
        }
    }
}

void lireMemo(unsigned char **cells, DimTableaux dim, int *vivantes)
{
    for (unsigned int y = 0; y < dim.hauteur; y++) {
        for (unsigned int x = 0; x < dim.largeur; x++) {
            if(cells[y][x] & 0x40){
                set_cell(cells, dim, x, y);
                cells[y][x] |= 0x80;
                *vivantes+=1;
            }
        }
    }
}

/* Vérifie si tableau non vide. */
int tableauVide(unsigned char **tab, DimTableaux dim, char mask)
{
    for(unsigned int i = 0; i < dim.hauteur; i++){
        for(unsigned int j = 0; j < dim.largeur; j++){
            if(tab[i][j] & mask)
                return 0;
        }
    }
    return 1;
}

/* Mise à jour des dimensions aprés un zoom ou dézoom. */
void majDimension(DimTableaux *dim, Cellules *cellule)
{
    dim->debMargeL = (LARGEUR_GRILLE/CELLULE - (LARGEUR_GRILLE/cellule->type)) /2;
    dim->finMargeL = (LARGEUR_GRILLE/CELLULE) - dim->debMargeL;
    dim->debMargeH = (HAUTEUR_GRILLE/CELLULE - (HAUTEUR_GRILLE/cellule->type)) /2;
    dim->finMargeH = (HAUTEUR_GRILLE/CELLULE) - dim->debMargeH;

    if(cellule->type > 1)
        cellule->position.w = cellule->position.h = cellule->type - 1;
    else
        cellule->position.w = cellule->position.h = cellule->type;
}

/* Active une cellule off en incrémentant le nombre de voisins activés
 * pour les huit cellules voisines */
void set_cell(unsigned char **cells, DimTableaux dim, unsigned int x, unsigned int y)
{
   unsigned int w = dim.largeur, h = dim.hauteur;
   int xoleft, xoright, yoabove, yobelow;
   unsigned char *cell_ptr = cells[y] + x;

   // Calculate the offsets to the eight neighboring cells,
   // accounting for wrapping around at the edges of the cell map

    if (x == 0)
        xoleft = w - 1;
    else
        xoleft = -1;
    if (y == 0)
        yoabove = (w * h) - w;
    else
        yoabove = -w;
    if (x == (w - 1))
        xoright = -(w - 1);
    else
        xoright = 1;
    if (y == (h - 1))
        yobelow = -((w*h) - w);
    else
        yobelow = w;

    *(cell_ptr) |= 0x01;
    *(cell_ptr + yoabove + xoleft) += 2;
    *(cell_ptr + yoabove) += 2;
    *(cell_ptr + yoabove + xoright) += 2;
    *(cell_ptr + xoleft) += 2;
    *(cell_ptr + xoright) += 2;
    *(cell_ptr + yobelow + xoleft) += 2;
    *(cell_ptr + yobelow) += 2;
    *(cell_ptr + yobelow + xoright) += 2;
}

/* Désactive une cellule ON, décrémentant le nombre de voisins activés
 * pour les huit cellules voisines */
void clear_cell(unsigned char **cells, DimTableaux dim, unsigned int x, unsigned int y)
{
    unsigned int w = dim.largeur, h = dim.hauteur;
    int xoleft, xoright, yoabove, yobelow;
    unsigned char *cell_ptr = cells[y] + x;

    // Calculate the offsets to the eight neighboring cells,
    // accounting for wrapping around at the edges of the cell map
    if (x == 0)
        xoleft = w - 1;
    else
        xoleft = -1;
    if (y == 0)
        yoabove = (w * h) - w;
    else
        yoabove = -w;
    if (x == (w - 1))
        xoright = -(w - 1);
    else
        xoright = 1;
    if (y == (h - 1))
        yobelow = -((w * h) - w);
    else
        yobelow = w;

    *(cell_ptr) &= ~0x01;
    *(cell_ptr + yoabove + xoleft) -= 2;
    *(cell_ptr + yoabove ) -= 2;
    *(cell_ptr + yoabove + xoright) -= 2;
    *(cell_ptr + xoleft) -= 2;
    *(cell_ptr + xoright) -= 2;
    *(cell_ptr + yobelow + xoleft) -= 2;
    *(cell_ptr + yobelow) -= 2;
    *(cell_ptr + yobelow + xoright) -= 2;
}

/* Returns cell state (1=on or 0=off). */
int cell_state(unsigned char **cells, int x, int y)
{
    return cells[y][x] & 0x01;
}

/* Calculates and displays the next generation of current_map */
void genererNextCellules(unsigned char **cells, DimTableaux dim, Cellules *cel, SDL_Renderer *ren)
{
    unsigned int x, y, count;

    for (y = 0; y < dim.hauteur; y++) {
        for (x = 0; x < dim.largeur; x++) {

            if(!(cells[y][x] & 0x1F))
                continue;

            count = (cells[y][x] & 0x1E)>>1;

            if (cells[y][x] & 0x01) {
                // Cell is on; turn it off if it doesn't have 2 or 3 neighbors
                if ((count != 2) && (count != 3)) {
                    cells[y][x] &= ~0x80;
                    cel->nbVivantes--;
                }
                else{
                    if(y >= dim.debMargeH && y < dim.finMargeH &&\
                       x >= dim.debMargeL && x < dim.finMargeL){
                            cel->position.x = (x-dim.debMargeL)*cel->type;
                            cel->position.y = (y-dim.debMargeH)*cel->type;
                            SDL_RenderCopy(ren, cel->vivante, NULL, &cel->position);
                    }
                }
            }
            else{
                // Cell is off; turn it on if it has exactly 3 neighbors
                if (count == 3) {
                    cells[y][x] |= 0x80;
                    cel->nbVivantes++;
                    if(y >= dim.debMargeH && y < dim.finMargeH &&\
                       x >= dim.debMargeL && x < dim.finMargeL){
                            cel->position.x = (x-dim.debMargeL) * cel->type;
                            cel->position.y = (y-dim.debMargeH) * cel->type;
                            SDL_RenderCopy(ren, cel->vivante, NULL, &cel->position);
                    }
                }
            }
        }
    }
    copyBits(cells, dim);
}

void copyBits(unsigned char **cells, DimTableaux dim)
{
    for (unsigned int y = 0; y < dim.hauteur; y++) {
        for (unsigned int x = 0; x < dim.largeur; x++) {
            if((cells[y][x] & 0x01) != (cells[y][x] & 0x80)>>7){
                if(cells[y][x] & 0x80)
                    set_cell(cells, dim, x, y);
                else
                    clear_cell(cells, dim, x, y);
            }
        }
    }
}

/* Affiche des cellules aléatoirement. */
void genererCellulesAleatoires(unsigned char **cells, DimTableaux dim, int *nbVivantes)
{
    unsigned int x, y, init_length;
    init_length = ((dim.finMargeH - dim.debMargeH) * (dim.finMargeL - dim.debMargeL)) / 4;

    do {
      x = rand()%(dim.finMargeL - dim.debMargeL) + dim.debMargeL;
      y = rand()%(dim.finMargeH - dim.debMargeH) + dim.debMargeH;
      if(cell_state(cells, x, y)==0){
        set_cell(cells, dim, x, y);
        cells[y][x] |= 0x80;
        *nbVivantes+=1;
      }
   } while (--init_length);
}
