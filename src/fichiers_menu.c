#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string.h>
#include "const.h"
#include "struc.h"
#include "fichiers_menu.h"


/* Détermine la taille d'une case du menu en largeur et hauteur. */
int tailleCaseMenu(char **tab, int *menuW, int *menuH, int taille)
{
    TTF_Font * font = TTF_OpenFont(CHEMIN_FONT, TAILLE_LETTRES);
    if(font == NULL){
        printf("Erreur font");
        goto Erreur;
    }

    int i, w, h;
    *menuW = *menuH = w = h = 0;

    for(i = 0; i < taille; i++)
    {
        if(0 != TTF_SizeText(font, nom(tab[i]), &w, &h)){
            printf("Erreur TTF_SizeText\n");
            goto Erreur;
        }
        if(*menuW < w)
            *menuW = w;
        if(*menuH < h)
            *menuH = h;
    }

    TTF_CloseFont(font);

    *menuW += 2*MARGE;

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fct tailleCaseMenu, fichiers_menu.c : %s\n", SDL_GetError());
    return -1;
}

/* En-tête menu */
int enteteMenu(MenuDyn *menu, SDL_Renderer *r, TTF_Font *font)
{
    //fondMenu(r);
    SDL_Rect pos = {POSX_MENU(menu->menuW) - MARGE, POSY_MENU - menu->menuH,
                    menu->menuW, menu->menuH};

    SDL_Surface *surf = NULL;
    SDL_Texture *text = NULL;

    char entete[30] = "";

    int nombrePage = (menu->nbFichiers / ELEMENTS_MAX) + 1;
    int pageActuelle = (menu ->cases / ELEMENTS_MAX) + 1;

    if(sprintf(entete, "Page %d/%d", pageActuelle, nombrePage) < 0)
        printf("Erreur sprintf\n");

    if(0 != SDL_SetRenderDrawColor(r, menu->bg.r, menu->bg.g, menu->bg.b, menu->bg.a)) goto Erreur;
    if(0 != SDL_RenderFillRect(r, &pos)) goto Erreur;

    surf = TTF_RenderUTF8_Shaded(font, entete, menu->fg, menu->bg);
    if(NULL == surf) goto Erreur;

    text = SDL_CreateTextureFromSurface(r, surf);
    if(NULL == text) goto Erreur;

    pos.w = surf->w;
    pos.x += (menu->menuW - surf->w) / 2;

    if(0 != SDL_RenderCopy(r, text, NULL, &pos)) goto Erreur;

    SDL_SetRenderDrawColor(r, 25, 35, 41, 255);
    SDL_RenderDrawLine(r, POSX_MENU(menu->menuW) - MARGE, (POSY_MENU-1),
                          POSX_MENU(menu->menuW) + menu->menuW, (POSY_MENU-1));

    SDL_FreeSurface(surf);
    SDL_DestroyTexture(text);

    return 0;
Erreur:
    if(surf != NULL) SDL_FreeSurface(surf);
    if(text != NULL) SDL_DestroyTexture(text);
    fprintf(stderr, "Erreur SDL, fct enteteMenu, fichiers_menu.c : %s\n", SDL_GetError());
    return -1;
}

/* Affiche une page menu */
int pageMenu(MenuDyn *menu, SDL_Renderer *r, TTF_Font *font)
{
    if(0 != SDL_SetRenderDrawColor(r, menu->bg.r, menu->bg.g, menu->bg.b, menu->bg.a)) goto Erreur;
    if(0 != SDL_RenderFillRect(r, &menu->fondM)) goto Erreur;

    int i = menu->deb;

    for(; i < menu->fin; i++){
        SDL_Surface *surf = NULL;
        SDL_Texture *text = NULL;

        surf = TTF_RenderUTF8_Shaded(font, nom(menu->cheminFichier[i]), menu->fg, menu->bg);
        if(NULL == surf) goto Erreur;

        text = SDL_CreateTextureFromSurface(r, surf);
        if(NULL == text) { SDL_FreeSurface(surf); goto Erreur; }

        menu->posM.w = surf->w;
        if(0 != SDL_RenderCopy(r, text, NULL, &menu->posM)){
            SDL_FreeSurface(surf);
            SDL_DestroyTexture(text);
            goto Erreur;
        }
        menu->posM.y += menu->menuH;

        SDL_FreeSurface(surf);
        SDL_DestroyTexture(text);

    }
    menu->posM.y = POSY_MENU;

    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fct pageMenu, fichiers_menu.c : %s\n", SDL_GetError());
    return -1;
}

int choixMenu(MenuDyn *menu, SDL_Renderer *ren, TTF_Font *font)
{
    SDL_Color bg = {128, 139, 150, 255};
    SDL_SetRenderDrawColor(ren, bg.r, bg.g, bg.b, bg.a);
    SDL_RenderFillRect(ren, &menu->survolM);
    int i = 0;
    if(menu->survolM.y > POSY_MENU)
        i = (menu->survolM.y - POSY_MENU) / menu->menuH;
    i += menu->deb;
    menu->cases = i;

    SDL_Surface *surf = TTF_RenderUTF8_Shaded(font, nom(menu->cheminFichier[i]), menu->fg, bg);
    if(NULL == surf) goto Erreur;

    SDL_Texture *text = SDL_CreateTextureFromSurface(ren, surf);
    if(NULL == text) goto Erreur;

    menu->survolM.x += MARGE;
    menu->survolM.w = surf->w;

    SDL_RenderCopy(ren, text, NULL, &menu->survolM);

    SDL_FreeSurface(surf);
    SDL_DestroyTexture(text);
    menu->survolM.x -= MARGE;
    menu->survolM.w = menu->menuW;

    return 0;

Erreur:
    if(surf != NULL) SDL_FreeSurface(surf);
    if(text != NULL) SDL_DestroyTexture(text);
    fprintf(stderr, "Erreur SDL, fct enteteMenu, fichiers_menu.c : %s\n", SDL_GetError());
    return -1;
}

/* Position chaine sans le chemin (ou en partie) */
char *nom(const char *chaine)
{
    char *p = NULL;
    p = strrchr(chaine, '/') + 1;

    if(p != NULL)
        return p;
    else
        return "Erreur Fct nom()";
}
