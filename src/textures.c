#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <locale.h>
#include <libintl.h>
#include "const.h"
#include "bouton.h"
#include "textures.h"


/* Base fenêtre avec boutons et textes */
SDL_Texture *constrTabDeBord(SDL_Renderer *rendu)
{
    SDL_Texture *texture = SDL_CreateTexture(rendu, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                LARGEUR_FENETRE - LARGEUR_GRILLE, HAUTEUR_FENETRE - (HAUTEUR_FENETRE - HAUTEUR_GRILLE));
    if(NULL == texture) goto Erreur;

    if(0 != SDL_SetRenderTarget(rendu, texture)) goto Erreur;

    /* Colore la texture */
    if(SDL_SetRenderDrawColor(rendu, 242, 243, 244, 255) < 0) goto Erreur;
    if(SDL_RenderFillRect(rendu, NULL) < 0) goto Erreur;

    /* Cadres, boutons et titres, textes. */
    if(0 != cadreBouton(rendu)) goto Erreur;
    if(0 != coloriseTitreCadre(rendu)) goto Erreur;
    if(0 != boutonsNormaux(rendu)) goto Erreur;
    if(0 != texteFenetre(rendu)) goto Erreur;

    /* Postionne */
    SDL_Rect pos = {CADRE_X - LARGEUR_GRILLE + ESP_RADIO_XA, 0, BOUTON_RADIO, BOUTON_RADIO};

    /* dessine les boutons radios */
    if(SDL_SetRenderDrawColor(rendu, 255, 255, 255, 255) < 0) goto Erreur;

    for(int i = 0; i < NB_RADIO; i ++)
    {
        pos.y = CADRE4_Y + ESP_RADIO_Y;
        if(SDL_RenderFillRect(rendu, &pos) < 0) goto Erreur;

        pos.y = CADRE5_Y + ESP_RADIO_Y;
        if(SDL_RenderFillRect(rendu, &pos) < 0) goto Erreur;

        pos.x += ESP_RADIO_XB + BOUTON_RADIO;
    }

    if(0 != SDL_SetRenderTarget(rendu, NULL)) goto Erreur;

    return texture;
Erreur:
    if(NULL != texture)
        SDL_DestroyTexture(texture);
    fprintf(stderr, "Erreur SDL, fct constrTab§DeBord, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

SDL_Texture *barre(SDL_Renderer *rendu)
{
    SDL_Texture *texture = SDL_CreateTexture(rendu, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                LARGEUR_FENETRE, HAUTEUR_FENETRE - HAUTEUR_GRILLE);

    if(NULL == texture) goto Erreur;

    SDL_Rect pos = {0, BORDURE, LARGEUR_FENETRE, HAUTEUR_FENETRE - HAUTEUR_GRILLE - BORDURE};

    if(0 != SDL_SetRenderTarget(rendu, texture)) goto Erreur;

    if(SDL_SetRenderDrawColor(rendu, 242, 243, 244, 255) < 0) goto Erreur;
    if(SDL_RenderFillRect(rendu, NULL) < 0) goto Erreur;

    if(SDL_SetRenderDrawColor(rendu, 213, 216, 220, 255) < 0) goto Erreur;
    if(SDL_RenderFillRect(rendu, &pos) < 0) goto Erreur;

    if(SDL_SetRenderDrawColor(rendu, 128, 139, 150, 255) < 0) goto Erreur;
    if(0 != SDL_RenderDrawLine(rendu, 0, BORDURE, LARGEUR_FENETRE, BORDURE))
        goto Erreur;
    if(0 != SDL_RenderDrawLine(rendu, LARGEUR_FENETRE - LARG_BOUTON_AIDE, BORDURE,
                                      LARGEUR_FENETRE - LARG_BOUTON_AIDE,
                                      HAUTEUR_FENETRE - HAUTEUR_GRILLE + BORDURE))
        goto Erreur;
    if(0 != SDL_RenderDrawLine(rendu, 50, BORDURE, 50, HAUTEUR_FENETRE - HAUTEUR_GRILLE + BORDURE))
        goto Erreur;

    if(0 != texteBarre(rendu)) goto Erreur;

    if(0 != SDL_SetRenderTarget(rendu, NULL)) goto Erreur;

    return texture;

Erreur:
    if(NULL != texture)
        SDL_DestroyTexture(texture);
    fprintf(stderr, "Erreur SDL, fonction barre, fichier textures.c : %s\n", SDL_GetError());
    return NULL;

}

/* Crée les textures des grilles vides. */
SDL_Texture *grilles(SDL_Renderer *rendu, const char cellule)
{
    SDL_Rect pos = {0, 0, cellule-1, cellule-1};

    /* Création de la texture */
    SDL_Texture *texture = NULL;
    texture = SDL_CreateTexture(rendu, SDL_PIXELFORMAT_RGBA8888,
                                SDL_TEXTUREACCESS_TARGET, LARGEUR_GRILLE, HAUTEUR_GRILLE);
    if(NULL == texture) goto Erreur;

    /* Dessine sur la texture */
    if(SDL_SetRenderTarget(rendu, texture) < 0) goto Erreur;

    /* Fond texture */
    if(cellule > 6){
        if(SDL_SetRenderDrawColor(rendu, 86, 101, 115, 255) < 0) goto Erreur; /* Midnight blue */
    }
    if(cellule > 3){
        if(SDL_SetRenderDrawColor(rendu, 44, 62, 80, 255) < 0) goto Erreur; /* Midnight blue */
    }
    else if(cellule == 3){
        if(SDL_SetRenderDrawColor(rendu, 39, 55, 70, 255) < 0) goto Erreur; /* Midnight blue */
    }
    else if(cellule == 2){
        if(SDL_SetRenderDrawColor(rendu, 33, 47, 61, 255) < 0) goto Erreur; /* Midnight blue */
    }
    else{
        if(SDL_SetRenderDrawColor(rendu, 28, 40, 51, 255) < 0) goto Erreur; /* Midnight blue */
    }
    if(SDL_RenderFillRect(rendu, NULL) < 0) goto Erreur;

    /* Couleur carrés */
    if(SDL_SetRenderDrawColor(rendu, 23, 32, 42, 255) < 0) goto Erreur;   /* Midnight blue */

    /* Initialise la taille et positions des cellules */
    if(cellule > 1){
        int i, j;
        for (i = 0; i < HAUTEUR_GRILLE / cellule; i++)
        {
            for (j = 0; j < LARGEUR_GRILLE / cellule; j++)
            {
                pos.x = j * cellule;
                pos.y = i * cellule;
                if(SDL_RenderFillRect(rendu, &pos)) goto Erreur;
            }
        }
    }

    /* Dessine un point rouge au centre de la grille la plus zoomée*/
    if(cellule > 6){
        if(SDL_SetRenderDrawColor(rendu, 255, 0, 0, 255) < 0) goto Erreur;

        if(SDL_RenderDrawPoint(rendu, LARGEUR_GRILLE / 2 - 1, HAUTEUR_GRILLE / 2 - 1) < 0)
            goto Erreur;
    }

    if(SDL_SetRenderTarget(rendu, NULL) < 0) goto Erreur;

    return texture;

Erreur:
    fprintf(stderr, "Erreur SDL, fonction grille, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

/* Crée les textures des cellules, vivante. */
SDL_Texture *cellules(SDL_Renderer *rendu)
{
    SDL_Texture *texture = NULL;
    texture = SDL_CreateTexture(rendu, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                CELLULE, CELLULE);
    if(NULL == texture) goto Erreur;

    if(SDL_SetRenderTarget(rendu, texture) < 0) goto Erreur;

    if(SDL_SetRenderDrawColor(rendu, 235, 245, 251, 255) < 0) /* Peter river */
        goto Erreur;

    if(SDL_RenderFillRect(rendu, NULL) < 0) goto Erreur;

    if(SDL_SetRenderTarget(rendu, NULL) < 0) goto Erreur;

    return texture;
Erreur:
    fprintf(stderr, "Erreur SDL, Fonction cellule, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

SDL_Texture *boutons(SDL_Renderer *renderer, const char etat)
{
    SDL_Texture *texture = NULL;
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                LARG_BOUTON,HAUT_BOUTON );
    if(NULL == texture)
        goto Erreur;

    if(SDL_SetRenderTarget(renderer, texture) < 0)
        goto Erreur;

    if(etat){ /* Cliqué */
        if(SDL_SetRenderDrawColor(renderer, 174, 182, 191, 255) < 0)
            goto Erreur;
    }
    else { /* Survol */
        if(SDL_SetRenderDrawColor(renderer, 141, 154, 165, 255) < 0)
        //if(SDL_SetRenderDrawColor(renderer, 146, 160, 170, 255) < 0)
            goto Erreur;
    }

    if(SDL_RenderFillRect(renderer, NULL) < 0) goto Erreur;

    if(SDL_SetRenderTarget(renderer, NULL) < 0) goto Erreur;

    return texture;
Erreur:
    fprintf(stderr, "Erreur SDL, fonction boutons, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

SDL_Texture *btRadio(SDL_Renderer *renderer, const char couleur)
{
    SDL_Texture *texture = NULL;
    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                BOUTON_SELEC, BOUTON_SELEC);

    if(NULL == texture) goto Erreur;

    if(SDL_SetRenderTarget(renderer, texture) < 0) goto Erreur;

    if(couleur){ /* zoom */
        if(0 != SDL_SetRenderDrawColor(renderer, 174, 214, 241, 255)) /* Marron */
            goto Erreur;

    }
    else { /* vitesse */
        if(0 != SDL_SetRenderDrawColor(renderer, 247, 203, 187, 255)) /* Bleu */
            goto Erreur;
    }

    if(SDL_RenderFillRect(renderer, NULL) < 0) goto Erreur;
    if(SDL_SetRenderTarget(renderer, NULL) < 0) goto Erreur;

    return texture;
Erreur:
    fprintf(stderr, "Erreur SDL, fonction btRadio, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

/* Bouton survol Quit. */
SDL_Texture *quit(SDL_Renderer *renderer)
{
    SDL_Texture *texture = NULL;

    texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                LARG_BOUTON, HAUT_BOUTON);
    if(NULL == texture) goto Erreur;

    if(0 != SDL_SetRenderDrawColor(renderer, 210,77, 87, 255)) goto Erreur;
    if(0 != SDL_SetRenderTarget(renderer, texture)) goto Erreur;
    if(0 != SDL_RenderFillRect(renderer, NULL)) goto Erreur;

    if(0 != SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255)) goto Erreur;

    logoBoutonQuit(renderer, LARG_BOUTON/2, HAUT_BOUTON/2, 5);

    if(0 != SDL_SetRenderTarget(renderer, NULL)) goto Erreur;

    return texture;
Erreur:
    fprintf(stderr, "Erreur SDL, fonction quit, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

/* Texture qui contiendra les chiffres de 0 à 9 pour l'affichage dynamique des générations. */
SDL_Texture *genChiffre(SDL_Renderer *renderer)
{
    TTF_Font * font = NULL;
    SDL_Surface * surf = NULL;
    SDL_Texture * tex = NULL;

    SDL_Color fg  = {228, 245, 255, 255}, bg = {93, 109, 126, 255};

    font = TTF_OpenFont(CHEMIN_FONT, TAILLE_CHIFFRES);
    if(NULL == font) goto Erreur;

    surf = TTF_RenderUTF8_Shaded(font, "0123456789", fg, bg);
    if(NULL == surf) goto Erreur;

    tex = SDL_CreateTextureFromSurface(renderer, surf);
    if(NULL == tex) goto Erreur;

    TTF_CloseFont(font);
    SDL_FreeSurface(surf);

    return tex;
Erreur:
    printf("Erreur SDL Fc texChiffre, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

/* Texture qui contiendra 'à propos' et l'aide. */
SDL_Texture *aideTex(SDL_Renderer *renderer)
{
    TTF_Font * font = NULL;
    SDL_Color cl = {235, 237, 239, 255};
    int espacement = 21, y = 0;

    SDL_Texture *texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET,
                                LARGEUR_GRILLE, HAUTEUR_GRILLE);
    if(NULL == texture) goto Erreur;

    /* Création d'une texture transparente. */
    SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND);

    if(0 != SDL_SetRenderTarget(renderer, texture)) goto Erreur;
    if(0 != SDL_SetRenderDrawColor(renderer, 33, 47, 60, 240)) goto Erreur;
    if(0 != SDL_RenderFillRect(renderer, NULL)) goto Erreur;

    /* Dessine les lignes séparatrices horizontales. */
    if(0 != SDL_SetRenderDrawColor(renderer, 214, 219, 223, 255)) goto Erreur;
    if(0 != SDL_RenderDrawLine(renderer, 150, 104, 500, 104)) goto Erreur;
    if(0 != SDL_RenderDrawLine(renderer, 150, 230, 500, 230)) goto Erreur;
    if(0 != SDL_RenderDrawLine(renderer, 150, 410, 500, 410)) goto Erreur;
    /* Verticale. */
    if(0 != SDL_RenderDrawLine(renderer, (LARGEUR_GRILLE/2)-100, 272, (LARGEUR_GRILLE/2)-100, 390)) goto Erreur;
    if(0 != SDL_RenderDrawLine(renderer, (LARGEUR_GRILLE/2)+100, 272, (LARGEUR_GRILLE/2)+100, 390)) goto Erreur;

    font = TTF_OpenFont(CHEMIN_FONT, 11);
    if(NULL == font) goto Erreur;

    /* Ecrit les 3 titres en gras*/
    TTF_SetFontStyle(font, TTF_STYLE_BOLD);

    ecrireTexte(295, 15, renderer, cl, font, _("À propos"));
    ecrireTexte(265, 115, renderer, cl, font, _("Boutons principaux"));
    ecrireTexte(300, 242, renderer, cl, font, _("Clavier"));

    TTF_SetFontStyle(font, TTF_STYLE_NORMAL);

    /* À propos. */
    ecrireTexte(50, 35, renderer, cl, font, _("Programme :  vie01"));
    ecrireTexte(200, 35, renderer, cl, font, VERSION);
    ecrireTexte(50, 35 + espacement, renderer, cl, font, _("Auteur :  Bruno"));
    ecrireTexte(200, 35 + espacement, renderer, cl, font, _("Site :  https://gitlab.com/brunoy/vie01"));
    ecrireTexte(50, 35 + (2*espacement), renderer, cl, font, _("Description :  Le jeu de la vie est un automate\
 cellulaire imaginé par John Horton Conway en 1970."));


    /* Boutons principaux */

    y = 138;
    ecrireTexte(50, y, renderer, cl, font, _("Afficher"));
    ecrireTexte(140, y, renderer, cl, font, _(":  menu contenant les patterns."));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, _("Sauver"));
    ecrireTexte(140, y, renderer, cl, font, _(":  sauvegarde une figure créée au format RLE."));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, _("Dernier joué"));
    ecrireTexte(140, y, renderer, cl, font, _(": affiche la dernière figure créée et lancée."));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, _("Stop|1"));
    ecrireTexte(140, y, renderer, cl, font, _(": stoppe les générations ou lance une génération (pas de 1)."));

    /* Clavier - 1ere colonne. */
    //y += 50 + espacement;
    y = 272;
    ecrireTexte(50, y, renderer, cl, font, _("ESPACE"));
    ecrireTexte(105, y, renderer, cl, font, _(": lancer / pause"));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, ";");
    ecrireTexte(105, y, renderer, cl, font, _(": générer par pas de 1"));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, "< / >");
    ecrireTexte(105, y, renderer, cl, font, _(": zoom "));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, "- / +");
    ecrireTexte(105, y, renderer, cl, font, _(": vitesse"));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, "!");
    ecrireTexte(105, y, renderer, cl, font, _(": aléatoire"));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, _("Suppr"));
    ecrireTexte(105, y, renderer, cl, font, _(": effacer"));

    /* Clavier - 2eme colonne. */
    y = 272;
    ecrireTexte(235, y, renderer, cl, font, _("Flêche haut, bas,"));
    ecrireTexte(330, y, renderer, cl, font, _(": déplacement ..."));

    y += espacement;
    ecrireTexte(235, y, renderer, cl, font, _("droite, gauche"));
    ecrireTexte(330, y, renderer, cl, font, _(": ... carte"));

    y += espacement;
    ecrireTexte(235, y, renderer, cl, font, "Q");
    ecrireTexte(330, y, renderer, cl, font, _(": quitter"));

    /* Clavier - 3eme colonne. */
    TTF_SetFontStyle(font, TTF_STYLE_ITALIC);
    y = 270;
    ecrireTexte(440, y, renderer, cl, font, _("Controle du menu : ......................."));
    TTF_SetFontStyle(font, TTF_STYLE_NORMAL);


    y += espacement+2;
    ecrireTexte(435, y, renderer, cl, font, _("Page haut"));
    ecrireTexte(510, y, renderer, cl, font, _(": page précédente"));

    y += espacement;
    ecrireTexte(435, y, renderer, cl, font, _("Page bas"));
    ecrireTexte(510, y, renderer, cl, font, _(": page suivante"));

    y += espacement;
    ecrireTexte(435, y, renderer, cl, font, _("Flêche haut"));
    ecrireTexte(510, y, renderer, cl, font, _(": sélection haut"));

    y += espacement;
    ecrireTexte(435, y, renderer, cl, font, _("Flêche bas"));
    ecrireTexte(510, y, renderer, cl, font, _(": sélection bas"));

    y += espacement;
    ecrireTexte(435, y, renderer, cl, font, _("Entrée"));
    ecrireTexte(510, y, renderer, cl, font, _(": sélectionner"));

    ecrireTexte(50, y += 50, renderer, cl, font,
                _("** Déplacement souris + appui bouton gauche sur la grille : remplissage."));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font,
                _("** Déplacement souris + appui bouton droit sur la grille : déplace la carte (si zoomée)."));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font,
                _("** Le centre est repéré par un pixel rouge sur la grille la  +  zoomé ."));

    y += espacement;
    ecrireTexte(50, y, renderer, cl, font, _("** Les cartes zoomées peut être déplacées.\
 Si on change le zoom, la carte est recentrée."));

#ifdef __linux__
    ecrireTexte(50, y += espacement, renderer, cl, font, _("** Le programme creait un dossier caché\
 '.vie01' à la racine du home. Des patterns peuvent être ajouter dans le"));
    ecrireTexte(50, y += espacement, renderer, cl, font, _("   dossier '.vie01/patterns'."));
    ecrireTexte(50, y += espacement, renderer, cl, font, _("** Le répertoire '.vie01/patterns/auser'\
 contiendra les figures personnelles enregistrées."));
#endif

    if(0 != SDL_SetRenderTarget(renderer, NULL)) goto Erreur;

    TTF_CloseFont(font);
    return texture;
Erreur:
    if(NULL != font)
        TTF_CloseFont(font);
    if(NULL != texture)
        SDL_DestroyTexture(texture);
    fprintf(stderr, "Erreur SDL, fonction aideTex, fichier aide.c : %s\n", SDL_GetError());
    return NULL;
}

/* Pour écrire les textes. */
SDL_Texture *text(SDL_Renderer *renderer, SDL_Color couleur, TTF_Font *font, SDL_Rect *pos, const char *texte)
{
    SDL_Surface * surf = NULL;
    SDL_Texture * tex = NULL;

    surf = TTF_RenderUTF8_Blended(font, texte, couleur);
    if(NULL == surf)
        goto Erreur;

    tex = SDL_CreateTextureFromSurface(renderer, surf);
    if(NULL == tex)
        goto Erreur;

    if(0 != SDL_QueryTexture(tex, NULL, NULL, &pos->w, &pos->h))
        goto Erreur;

    SDL_FreeSurface(surf);
    return tex;
Erreur:
    if(NULL != surf)
        SDL_FreeSurface(surf);
    if(NULL != tex)
        SDL_DestroyTexture(tex);
    fprintf(stderr, "Erreur SDL, fonction text, fichier textures.c : %s\n", SDL_GetError());
    return NULL;
}

SDL_Texture *creerTexNomFichier(SDL_Renderer *ren, SDL_Rect *bar, const char *texte, TTF_Font *font)
{
    SDL_Color fg = {33,47,61,255}, bg = {213,216,220,255};

    SDL_Surface *surf = NULL;
    SDL_Texture *text = NULL;

    surf = TTF_RenderUTF8_Shaded(font, texte, fg, bg);
    if(NULL == surf) goto Erreur;

    text = SDL_CreateTextureFromSurface(ren, surf);
    if(NULL == text) goto Erreur;

    bar->w = surf->w;
    bar->h = surf->h;

    SDL_FreeSurface(surf);

    return text;
Erreur:
    fprintf(stderr, "Erreur SDL, fct creerTexNomFichier, texture.c : %s\n", SDL_GetError());
    return NULL;
}

SDL_Texture *bord(SDL_Renderer *rendu, const int x, const int y)
{
    SDL_Texture *texture = NULL;
    texture = SDL_CreateTexture(rendu, SDL_PIXELFORMAT_RGBA8888, SDL_TEXTUREACCESS_TARGET, x, y);
    if(NULL == texture)
        goto Erreur;

    if(SDL_SetRenderTarget(rendu, texture) < 0) goto Erreur;

    if(SDL_SetRenderDrawColor(rendu, 242, 243, 244, 255) < 0) goto Erreur;
    if(SDL_RenderFillRect(rendu, NULL) < 0) goto Erreur;

    if(SDL_SetRenderTarget(rendu, NULL) < 0) goto Erreur;

    return texture;
Erreur:
    fprintf(stderr, "Erreur SDL, fct bord, texture.c : %s\n", SDL_GetError());
    return NULL;
}

int ecrireTexte(int x, int y, SDL_Renderer *ren, SDL_Color cl, TTF_Font *font, char *texte)
{
    SDL_Rect pos = {x, y, 0, 0};
    SDL_Texture *tmp = NULL;
    tmp = text(ren, cl, font, &pos, texte);
    if(tmp == NULL) goto Erreur;
    SDL_RenderCopy(ren, tmp, NULL, &pos);
    SDL_DestroyTexture(tmp);
    tmp = NULL;
    return 0;
Erreur:
    fprintf(stderr, "Erreur SDL, fonction aideTex, fichier aide.c : %s\n", SDL_GetError());
    return -1;
}
