#ifndef DEF_ACTION
#define DEF_ACTION
    void boutonAfficher(char *generer, char *etat_menu, char *etat_survol);
    void boutonAleatoire(unsigned char **map, DimTableaux dim, int *cpt, int *nbVivantes);
    void boutonStopOuUn(unsigned char **map, DimTableaux dim, Cellules *cel,
                        int *cpt, char *memo, SDL_Renderer *ren);
    void boutonDernierJeu(unsigned char **map, DimTableaux dim, int *cpt, int *vivantes);
    int boutonLancer(unsigned char **map, DimTableaux dim, char *memo);
    void boutonEffacer(unsigned char **map, DimTableaux dim, char *etat_aide,
                       char *generer, int *nbGenerations, int *nbVivantes);
    void boutonAide(SDL_Texture **aide, SDL_Renderer *ren, char *etat_aide);
    int boutonSauvegarder(DimTableaux dim, MenuDyn *menu, unsigned char **map, char *monRep,
                          char *nom, char *mess);
    int boutonRadioVitesse(Boutons *bouton, int *delai, int *tempo, const int n);
    int vitesseClavier(Boutons *bouton, int *delai, int *tempo, int const ordre);
    int boutonRadioZoom(Boutons *bouton, int *zoom, int const n);
    int zoomClavier(Boutons *bouton, int *zoom, int const ordre);
    void activeCellule(unsigned char **map, DimTableaux dim, int x, int y, int *nbVivantes);
    void desactiveCellule(unsigned char **map, DimTableaux dim, int x, int y, int *nbVivantes);
#endif
