#ifndef DEF_STRUC
#define DEF_STRUC

//typedef enum { NORMAL, RADIOV, RADIOZ } Genre;

typedef enum
{
    NON, BASE, SURVOL, QUIT, CLIC, ANNULER, VALIDER
} Rafraichir;

typedef enum
{
    AFFICHER = 1, ALEATOIRE, STOPPER, EFFACER,
    SAUVER, DERNIERJOUE, LANCER, QUITTER, AIDE
} NomBouton;

typedef struct{
    SDL_Window *fenetre;
} Fenetre;

typedef struct{
    SDL_Renderer *rendu;
    SDL_Texture *tableauDeBord;
    SDL_Texture *barreInfo;
    SDL_Texture *texture;
    SDL_Texture *bordH;
    SDL_Texture *bordG;
    SDL_Rect positionBarre;
    SDL_Rect positionTabBord;
} Rendu;

typedef struct{
    SDL_Texture *pixel1;
    SDL_Texture *pixel2;
    SDL_Texture *pixel3;
    SDL_Texture *pixel6;
    SDL_Texture *pixel9;
    SDL_Rect positionG;
} Grilles;

typedef struct
{
    SDL_Texture *vivante;
    SDL_Rect position;
    int nbVivantes;
    int type;
} Cellules;

typedef struct
{
    int selecRadioV[10];
    int selecRadioZ[5];
    SDL_Rect positionB[3];
    SDL_Texture *survol;
    SDL_Texture *clique;
    SDL_Texture *quitte;
    SDL_Texture *radioV;
    SDL_Texture *radioZ;
} Boutons;

typedef struct
{
    SDL_Texture *chiffres;
    SDL_Rect src;
    SDL_Rect dst;
    char car[11];
    int compteur;
    int delai;
} Iterations;

typedef struct
{
    //unsigned char **map;
    unsigned int debMargeL;
    unsigned int finMargeL;
    unsigned int debMargeH;
    unsigned int finMargeH;
    unsigned int largeur;
    unsigned int hauteur;
} DimTableaux;

typedef struct
{
    unsigned char **map;
} Tableaux;


typedef struct
{
    SDL_Rect srcT;
    SDL_Rect dstT;
    char zoneText[100];
    char zoneMess[30];
    char *pattHome;
} Sauvegarder;


typedef struct
{
    char **cheminFichier;
    SDL_Color fg;
    SDL_Color bg;
    SDL_Rect posM;
    SDL_Rect fondM;
    SDL_Rect survolM;
    int deb, fin, cases;
    int menuW;
    int menuH;
    int nbFichiers;
} MenuDyn;

typedef struct
{
    Fenetre sdl;
    Rendu base;
    Grilles grille;
    Boutons bouton;
    Cellules cellule;
    Iterations iter;
    MenuDyn *menu;
    Sauvegarder svg;
} Programme;

#endif
