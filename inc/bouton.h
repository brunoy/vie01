#ifndef DEF_BOUTON
#define DEF_BOUTON
    int logoBoutonQuit(SDL_Renderer *renderer, const int x_centre, const int y_centre, const int rayon);
    int etatRadio(SDL_Renderer *renderer, SDL_Rect *pos, char select);
    int cadreBouton(SDL_Renderer *renderer);
    int boutonsNormaux(SDL_Renderer *renderer);
    int coloriseTitreCadre(SDL_Renderer *renderer);
    int texteFenetre(SDL_Renderer *renderer);
    int texteBarre(SDL_Renderer *rendu);
#endif
