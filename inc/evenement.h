#ifndef DEF_EVENEMENT
#define DEF_EVENEMENT
    int clicSurGrille(int zoom, int margeH, int margeL, int *x, int *y);
    int survolBouton(SDL_Rect *position, char *etatSurvol, int x, int y);
    int clicSurBouton(SDL_Rect *position, int x, int y);
    int quelBouton(SDL_Rect *p, int y);
    int clicBoutonRadioVitesse(int x, int y);
    int clicBoutonRadioZoom(int x, int y);
    void survolMenu(MenuDyn *menu, char *etatSurvol, int x, int y);
    int fleche_up(SDL_Rect *survolM, int *deb, int *fin, int menuH);
    int fleche_down(SDL_Rect *survolM, int mMax, int nFic, int *deb, int *fin, int menuH);
    void molette(int y, int nbFics, int *deb, int *fin);
    void pageUp(int nbFics, int *deb, int *fin);
    void pageDw(int nbFics, int *deb, int *fin);
    int clicMenu(MenuDyn *menu, int x, int y, char *etatMenu);
    int survolSaisie(int x, int y, char *svg_survol);
    int clicSaisie(int x, int y, char *svg_survol);
    int deplacerCarte(DimTableaux *dim, int relX, int relY);
#endif
