#ifndef DEF_FICHIERS_SAUVER
#define DEF_FICHIERS_SAUVER

/* Largeur maxi conseillée pour les life.rle */
#define LARG_MAX_RLE    70

    int boiteDeSaisie(SDL_Renderer *renderer, char svg_survol);
    int boutonAnnuler(SDL_Renderer *renderer, char svg_survol);
    int boutonValider(SDL_Renderer *renderer, char svg_survol);
    void deplacementText(SDL_Rect *srcT, SDL_Rect *dstT);
    void ajoutCaractere(char *chaine, const char *car);
    void supprimerCaractere(char *chaine);
    void initChaine(char *chaine);
    void initMessage(char *message);
    int verifMaxChaine(const char *chaine, char *message);
    int texteBoiteSaisie(SDL_Renderer *renderer, TTF_Font *font, Sauvegarder *svg);
    char dir(const char *name);
    char *concat(const char *nom, const char *monRep);
    int verifNomPresent(char **cheminFichier, const char *monPattern, const int nbFichiers);
    int ecrireFichieRLE(DimTableaux dim, unsigned char **map, const char *nom, const char *monPattern);
    int celluleSuivante(unsigned char **map, int x, int y, int x1, int x2, int y2);
    void celluleDebutFin(unsigned char **map, DimTableaux dim, int *x1, int *y1, int *x2, int *y2);
    int ajoutPatternBase(MenuDyn *menu, char *pattern);
#endif
