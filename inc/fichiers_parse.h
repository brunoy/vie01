#ifndef DEF_FICHIERS_PARSE
#define DEF_FICHIERS_PARSE

#define NB_CARACTERES   90

    int choixFonctionParse(const char *fic, unsigned char **orig, char *texte, DimTableaux dim, int *nbVivantes);
    int compareExtension(const char * fileExt, const char * extension);
    void supprimeRetourLigne(char *chaine);
    void delSpace(char* str);
    int posY(char *chaine, int *y);
    int posX(char *chaine, int *x);
    int extraitNombre(char *p, int *n);
    int parseFichierLIF(const char *fic, unsigned char **orig, char *texte, DimTableaux dim, int *nbVivantes);
    int lignexyrRLE(char *chaine, unsigned int *x, unsigned int *y);
    int parseFichierRLE(const char *fic, unsigned char **orig, char *texte, DimTableaux dim, int *nbVivantes);
#endif
