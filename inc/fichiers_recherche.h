#ifndef DEF_FICHIERS_RECHERCHE
#define DEF_FICHIERS_RECHERCHE

#define LIMIT   256
    char isDir(const char *name);
    int verifExtension(const char * fileExt, const char * extension);
    int compterFichierLifRle(const char *dossier);
    int ParcourirFichiers(const char *dossier, char **tabChaine, int *index);
    int indiceNomMaxi(char **chaine, const int ligne);
    char *nomSansChemin(const char *chaine);
    char *nomSansExtension(const char *chaine);
    void libereMem(char **tableau, char *chaine, int indice);
    int compare (void const *a, void const *b);
#endif
