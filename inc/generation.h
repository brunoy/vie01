#ifndef DEF_GENERATION
#define DEF_GENERATION
    void initTab(unsigned char **cells, DimTableaux dim);
    void copieMemo(unsigned char **cells, DimTableaux dim);
    void lireMemo(unsigned char **cells, DimTableaux dim, int *vivantes);
    int tableauVide(unsigned char **tab, DimTableaux dim, char mask);
    void majDimension(DimTableaux *dim, Cellules *cellule);
    void set_cell(unsigned char **cells, DimTableaux dim, unsigned int x, unsigned int y);
    void clear_cell(unsigned char **cells, DimTableaux dim, unsigned int x, unsigned int y);
    int cell_state(unsigned char **cells, int x, int y);
    void genererNextCellules(unsigned char **cells, DimTableaux dim, Cellules *cel, SDL_Renderer *ren);
    void copyBits(unsigned char **cells, DimTableaux dim);
    void genererCellulesAleatoires(unsigned char **tab, DimTableaux dim, int *nbVivantes);

#endif
