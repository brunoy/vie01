#ifndef DEF_INIT
#define DEF_INIT
    void initLocales(void);
    int initSDL(Fenetre *creer, Rendu *renderer);
    int initBaseTexture(Rendu *base);
    int initBoutons(Boutons *bouton, SDL_Renderer *renderer);
    int initGrilles(Grilles *damier, SDL_Renderer *renderer);
    int initIterations(Iterations *iter, int vit, SDL_Renderer *renderer);
    int initCellules(Cellules *cell, SDL_Renderer *renderer);
    int initTableaux(Tableaux *tab);
    void initDimTableaux(DimTableaux *dim);
    int initSauver(Sauvegarder *svg);
    int initRepertLinux(char **pattHome);
    int initMenu(MenuDyn *menu, const char *monRep);
    SDL_Surface *image(void);
#endif
