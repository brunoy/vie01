#ifndef DEF_CONST
#define DEF_CONST

/* Fenêtre */
#define LARGEUR_FENETRE     800
#define HAUTEUR_FENETRE     600
#define LARGEUR_GRILLE      648   /* Taille divisible par 2, 3, 6, 9 avec un résultat pair. */
#define HAUTEUR_GRILLE      576
#define CELLULE_Z9          9     /* Cellule 9 pixels */
#define CELLULE_Z6          6     /* cellule 6 pixels */
#define CELLULE_Z3          3     /* cellule 3 pixels */
#define CELLULE_Z2          2     /* cellule 2 pixels */
#define CELLULE             1     /* Cellule base 1 pixel */
#define BORDURE             4

/* Menu */
#define MAX_FICHIERS        15000
#define ELEMENTS_MAX        20
#define POSX_MENU(w)        ((LARGEUR_GRILLE/2)-(w/2)) /* Macro */
#define POSY_MENU           31
#define MARGE               7
#define BARRE_W             10

/* -------------------------------------------------- */
/* Dimensions cadre des boutons. */
#define LARG_CADRE      136
#define HAUT_CADRE_A    35
#define HAUT_CADRE_B    39
#define HAUT_CADRE_C    38

/* Espacement entre cadres. */
#define ESP_CADRE   28

/* Positions cadres des boutons. */
#define CADRE_X     (LARGEUR_FENETRE - 144)
#define CADREG_Y    30                                              /* Génération */
#define CADREV_Y    (CADREG_Y + 46)                                 /* Cellule vivante */
#define CADRE2_Y    (CADREV_Y + HAUT_CADRE_A + ESP_CADRE + 5)
#define CADRE3_Y    (CADRE2_Y + HAUT_CADRE_B + ESP_CADRE + 10)
#define CADRE4_Y    (CADRE3_Y + HAUT_CADRE_B + ESP_CADRE + 10)
#define CADRE5_Y    (CADRE4_Y + HAUT_CADRE_B + ESP_CADRE)
#define CADRE6_Y    (CADRE5_Y + HAUT_CADRE_B + ESP_CADRE + 10)
#define CADRE7_Y    (CADRE6_Y + HAUT_CADRE_A + ESP_CADRE + 10)

/* --------------------------------------------------- */

/* Cadre coloré générations */
#define X_NOMBRE_GEN_PT (CADRE_X + 11)
#define Y_NOMBRE_GEN_PT (CADREG_Y + 8)
#define X_NOMBRE_GEN_LG 118
#define Y_NOMBRE_GEN_LG 19
/* Cadre coloré cellules vivantes */
#define Y_NOMBRE_VIV_PT (CADREV_Y + 8)

/* --------------------------------------------------- */

/* Cadres colorés "titre" */
#define LARG_CADRE_CL   120
#define HAUT_CADRE_CL   16
#define ESP_CL          2

/* --------------------------------------------------- */

/* Dimensions boutons rectangulaires. */
#define LARG_BOUTON   35
#define HAUT_BOUTON   15

/* Positions des boutons. */
#define BOUTON_GX     (CADRE_X + 20)
#define BOUTON_DX     (BOUTON_GX + LARG_BOUTON + 26)
#define ESP_BOUTON_Y    ((HAUT_CADRE_B - HAUT_BOUTON)/2)

/* Bouton aide. */
#define LARG_BOUTON_AIDE    25
#define BOUTON_AIDE_X       (LARGEUR_FENETRE - LARG_BOUTON_AIDE)
#define BOUTON_AIDE_Y       580

/* Dimensions boutons radio. */
#define BOUTON_RADIO  14
#define ESP_RADIO_Y 12

/* Radio  */
#define NB_RADIO       5
#define ESP_RADIO_XA   17
#define ESP_RADIO_XB   8

/* Dimensions boutons radio sélèctionnés. */
#define BOUTON_SELEC    8
#define BOUTON_SELECZ_X    (CADRE_X + ESP_RADIO_XA + 3)
#define BOUTON_SELECZ_Y    (CADRE4_Y + ESP_RADIO_Y + 3)
#define BOUTON_SELECV_X    (CADRE_X + ESP_RADIO_XA + 3)
#define BOUTON_SELECV_Y    (CADRE5_Y + ESP_RADIO_Y + 3)

/* Boite de saisie sauvegarder. */
#define BOITE_LARG      220
#define BOITE_HAUT      120
#define BOITE_X         ((LARGEUR_GRILLE/2) - (BOITE_LARG/2))
#define BOITE_Y         ((HAUTEUR_GRILLE/2) - (BOITE_HAUT/2))
#define ZONE_LARG       150
#define ZONE_HAUT       17
#define ZONE_X          (BOITE_X + ((BOITE_LARG - ZONE_LARG) / 2))
#define ZONE_Y          (BOITE_Y + ((BOITE_HAUT/3) - ZONE_HAUT))
#define BOITE_BT_Y      (BOITE_Y + (BOITE_HAUT - (BOITE_HAUT/3)))
#define BOITE_BT_BORD   55

/* ----------------------------------------------------- */

/* Fonte */
#define TAILLE_LETTRES      13
#define TAILLE_CHIFFRES     12

#ifdef _WIN32
    #define CHEMIN_FONT     "font/Lato-Regular.ttf"
    #define USRPATTERNS     "patterns/"
#endif // Defini par cmake pour linux

#define DOSPERSO    "/auser/"

/* Gettext */
#define _(STRING)   gettext(STRING)

#endif
