#ifndef DEF_FICHIERS_MENU
#define DEF_FICHIERS_MENU
    int tailleCaseMenu(char **, int *, int *, int);
    int enteteMenu(MenuDyn *, SDL_Renderer *, TTF_Font *);
    int pageMenu(MenuDyn *, SDL_Renderer *, TTF_Font *);
    int choixMenu(MenuDyn *, SDL_Renderer *, TTF_Font *);
    char *nom(const char *);
#endif
