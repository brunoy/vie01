#ifndef DEF_TEXTURES
#define DEF_TEXTURES
    SDL_Texture *constrTabDeBord(SDL_Renderer *rendu);
    SDL_Texture *barre(SDL_Renderer *rendu);
    SDL_Texture *grilles(SDL_Renderer *rendu, const char cellule);
    SDL_Texture *cellules(SDL_Renderer *rendu);
    SDL_Texture *boutons(SDL_Renderer *renderer, const char etat);
    SDL_Texture *btRadio(SDL_Renderer *renderer, const char couleur);
    SDL_Texture *quit(SDL_Renderer *renderer);
    SDL_Texture *genChiffre(SDL_Renderer *renderer);
    SDL_Texture *aideTex(SDL_Renderer *renderer);
    SDL_Texture *text(SDL_Renderer *, SDL_Color, TTF_Font *, SDL_Rect *, const char *);
    SDL_Texture *creerTexNomFichier(SDL_Renderer *, SDL_Rect *, const char *, TTF_Font *);
    SDL_Texture *bord(SDL_Renderer *rendu, const int x, const int y);
    int ecrireTexte(int x, int y, SDL_Renderer *, SDL_Color, TTF_Font *, char *texte);
#endif
