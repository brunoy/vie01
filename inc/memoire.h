#ifndef DEF_MEMOIRE
#define DEF_MEMOIRE
    int allocMem(unsigned char ***ptr);
    int allocMemChaine(char ***ptr, const int taille);
    int reallocMem(char ***ptr, const int taille);
#endif
