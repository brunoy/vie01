#ifndef DEF_AFFICHAGE
#define DEF_AFFICHAGE
    int afficherBaseFenetre(Rendu base, Grilles grille, const int zoom);
    int afficherBordsGrille(Rendu base);
    int afficherTabBits(unsigned char **cells, DimTableaux dim, Cellules cellule, SDL_Renderer *renderer);
    int afficherNombreCellVivantes(Iterations iter, const int nbVivantes, SDL_Renderer *renderer);
    int afficherNombreIteration(Iterations iter, SDL_Renderer *renderer);
    int afficherBoutonRadioSelect(Boutons bouton, SDL_Renderer *renderer);
    int afficherBarre(SDL_Texture *bar, SDL_Rect pos, SDL_Renderer *renderer);
    int misAJourAffichage(Programme jeu, SDL_Texture *tex, SDL_Rect pos);
#endif
