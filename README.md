# vie01
* Game of life, cellular automaton .
* Jeu de la vie, automate cellulaire imaginé par John Horton Conway en 1970.

## Manuel build pour Linux

### Dépendances compilation :
* cmake
* gcc
* libsdl2-dev
* libsdl2-ttf-dev
### Dépendances programme   :
* libsdl2
* libsdl2-ttf
### Dépendance optionnelle pour la traduction
* Gettext

Pour builder et installer vie01 sur votre système, ouvrir un terminal
dans le dossier des sources 'vie01-master' :
```
$ mkdir build && cd build
$ cmake ..
$ make
# make install
```
Pour désinstaller, ouvrir un terminal dans le dossier des sources 'vie01-master' :
```
$ cd build
# make uninstall
```

## Manuel programme
* [linux](https://gitlab.com/brunoy/vie01/wikis/Manuel-Linux)

## Screenshot
![2019_10_13_1.png](https://gitlab.com/brunoy/vie01/wikis/uploads/cd8809d73e669749b54571484a8a5da2/2019_10_13_1.png)
