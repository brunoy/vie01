# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid   ""
msgstr  "Project-Id-Version: 1.1.0\n"
        "Report-Msgid-Bugs-To: bruno (at) mailoo (dot) org\n"
        "POT-Creation-Date: 2019-11-13 00:30+0100\n"
        "PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
        "Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
        "Language-Team: LANGUAGE <LL@li.org>\n"
        "Language: \n"
        "MIME-Version: 1.0\n"
        "Content-Type: text/plain; charset=UTF-8\n"
        "Content-Transfer-Encoding: 8bit\n"

#: src/action.c:100
msgid   "Carte vide"
msgstr  ""

#: src/action.c:107
msgid   "Vous n'avez rien saisi"
msgstr  ""

#: src/action.c:114
msgid   "Erreur concat nom"
msgstr  ""

#: src/action.c:119
msgid   "Nom déjà présent !"
msgstr  ""

#: src/action.c:124
msgid   "Erreur verif si nom présent !"
msgstr  ""

#: src/action.c:130
msgid   "Pas de dossier perso !"
msgstr  ""

#: src/bouton.c:178
msgid   "Générations"
msgstr  ""

#: src/bouton.c:180
msgid   "Cellules vivantes"
msgstr  ""

#: src/bouton.c:182
msgid   "Patterns  /  Sauver"
msgstr  ""

#: src/bouton.c:184
msgid   "Aléatoire / Dernier joué"
msgstr  ""

#: src/bouton.c:186
msgid   "-        Zoom        +"
msgstr  ""

#: src/bouton.c:188
msgid   "-       Vitesse       +"
msgstr  ""

#: src/bouton.c:190
msgid   "Stop|1  /  Lancer"
msgstr  ""

#: src/bouton.c:192
msgid   "Effacer  /  Quitter"
msgstr  ""

#: src/textures.c:305
msgid   "À propos"
msgstr  ""

#: src/textures.c:306
msgid   "Boutons principaux"
msgstr  ""

#: src/textures.c:307
msgid   "Clavier"
msgstr  ""

#: src/textures.c:312
msgid   "Programme :  vie01"
msgstr  ""

#: src/textures.c:314
msgid   "Auteur :  Bruno"
msgstr  ""

#: src/textures.c:315
msgid   "Site :  https://gitlab.com/brunoy/vie01"
msgstr  ""

#: src/textures.c:316
msgid   "Description :  Le jeu de la vie est un automate cellulaire imaginé "
        "par John Horton Conway en 1970."
msgstr  ""

#: src/textures.c:323
msgid   "Afficher"
msgstr  ""

#: src/textures.c:324
msgid   ":  menu contenant les patterns."
msgstr  ""

#: src/textures.c:327
msgid   "Sauver"
msgstr  ""

#: src/textures.c:328
msgid   ":  sauvegarde une figure créée au format RLE."
msgstr  ""

#: src/textures.c:331
msgid   "Dernier joué"
msgstr  ""

#: src/textures.c:332
msgid   ": affiche la dernière figure créée et lancée."
msgstr  ""

#: src/textures.c:335
msgid   "Stop|1"
msgstr  ""

#: src/textures.c:336
msgid   ": stoppe les générations ou lance une génération (pas de 1)."
msgstr  ""

#: src/textures.c:341
msgid   "ESPACE"
msgstr  ""

#: src/textures.c:342
msgid   ": lancer / pause"
msgstr  ""

#: src/textures.c:346
msgid   ": générer par pas de 1"
msgstr  ""

#: src/textures.c:350
msgid   ": zoom "
msgstr  ""

#: src/textures.c:354
msgid   ": vitesse"
msgstr  ""

#: src/textures.c:358
msgid   ": aléatoire"
msgstr  ""

#: src/textures.c:361
msgid   "Suppr"
msgstr  ""

#: src/textures.c:362
msgid   ": effacer"
msgstr  ""

#: src/textures.c:366
msgid   "Flêche haut, bas,"
msgstr  ""

#: src/textures.c:367
msgid   ": déplacement ..."
msgstr  ""

#: src/textures.c:370
msgid   "droite, gauche"
msgstr  ""

#: src/textures.c:371
msgid   ": ... carte"
msgstr  ""

#: src/textures.c:375
msgid   ": quitter"
msgstr  ""

#: src/textures.c:380
msgid   "Controle du menu : ......................."
msgstr  ""

#: src/textures.c:385
msgid   "Page haut"
msgstr  ""

#: src/textures.c:386
msgid   ": page précédente"
msgstr  ""

#: src/textures.c:389
msgid   "Page bas"
msgstr  ""

#: src/textures.c:390
msgid   ": page suivante"
msgstr  ""

#: src/textures.c:393
msgid   "Flêche haut"
msgstr  ""

#: src/textures.c:394
msgid   ": sélection haut"
msgstr  ""

#: src/textures.c:397
msgid   "Flêche bas"
msgstr  ""

#: src/textures.c:398
msgid   ": sélection bas"
msgstr  ""

#: src/textures.c:401
msgid   "Entrée"
msgstr  ""

#: src/textures.c:402
msgid   ": sélectionner"
msgstr  ""

#: src/textures.c:405
msgid   "** Déplacement souris + appui bouton gauche sur la grille : "
        "remplissage."
msgstr  ""

#: src/textures.c:409
msgid   "** Déplacement souris + appui bouton droit sur la grille : déplace "
        "la carte (si zoomée)."
msgstr  ""

#: src/textures.c:413
msgid   "** Le centre est repéré par un pixel rouge sur la grille la  +  "
        "zoomé ."
msgstr  ""

#: src/textures.c:416
msgid   "** Les cartes zoomées peut être déplacées. Si on change le zoom, la "
        "carte est recentrée."
msgstr  ""

#: src/textures.c:420
msgid   "** Le programme creait un dossier caché '.vie01' à la racine du "
        "home. Des patterns peuvent être ajouter dans le"
msgstr  ""

#: src/textures.c:422
msgid   "   dossier '.vie01/patterns'."
msgstr  ""

#: src/textures.c:423
msgid   "** Le répertoire '.vie01/patterns/auser' contiendra les figures "
        "personnelles enregistrées."
msgstr  ""

#: src/fichiers_sauver.c:167
msgid   "Maxi caractères atteint !"
msgstr  ""

#: src/fichiers_sauver.c:179
msgid   "Entrez un nom"
msgstr  ""
